<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * All entity classes must be declared as such.
 *
 * @ORM\Entity
 * @ORM\Table(name="articles")
 */
class Articles extends \Kdyby\Doctrine\Entities\BaseEntity{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
     */
    public $id;
    
    /**
     * @ORM\Column(type="string")
     */
    public $title;
    
	/**
     * @ORM\Column(type="string", nullable = true)
     */
	public $introPhoto;
	
	/**
     * @ORM\Column(type="text")
     */
	public $introText;
	
	/**
     * @ORM\Column (type="boolean") 
     */
    public $published = FALSE;
	
	/**
     * @ORM\Column (type="datetime", nullable = true) 
     */
    public $publishedDate;
	
    /**
     * @ORM\Column (type="datetime") 
     */
    public $created;
	
	/**
     * @ORM\Column (type="datetime", nullable = true) 
     */
    public $accessDate;
	
	/**
     * @ORM\Column (type="datetime", nullable=true) 
     */
    public $modified;
	
	/**
     * @ORM\Column (type="text") 
     */
    public $text;
	
	/**
     * @ORM\Column (type="text") 
     */
	public $metaDescription;
	
	/**
     * @ORM\Column (type="text") 
     */
	public $metaKeywords;
}
    