<?php

/**
 * Description of GeocachingPresenter
 *
 * @author Administrator
 */

namespace App\AdminModule\Presenters;

use Nette\Utils\Html;

class GeocachingPresenter extends BasePresenter {

    private $locFile;
    private $addedCaches = 0;
    private $geocacheArray = NULL;

    /** @var \Components\Factories\IGeocachingProcessFormFactory @inject */
    public $geocachingProcessFormFactory;

    /** @var \Components\Factories\IGeocachingEditFormFactory @inject */
    public $geocachingEditFormFactory;

    /**
     * @var \GeocachingManager @inject
     */
    public $geocachingManager;

    /** @var \Helpers\Slovak @inject */
    public $slovak;

    /** @var \Helpers\Locations @inject */
    public $locations;

    /** @var \Components\Factories\IGeocachingFilterFactory @inject */
    public $geocachingFilterFactory;

    /** @var \Components\Factories\IStatsFilterFactory @inject */
    public $statsFilterFactory;

    private function createGeojson() {
        $geojsonFile = new \GeoJsonFile\GeoJsonFile('map.geojson');

        $geoCaches = $this->geocachingManager->getAll();

        foreach ($geoCaches as $geocache) {

            $point = new \GeoJson\Geometry\Point(array($geocache->lon, $geocache->lat));
            $properties = ['title' => $geocache->name, 'icon' => array('iconUrl' => '/images/gcTypes/' . $geocache->type->image, 'iconSize' => [16, 16], 'className' => 'dots')];

            $geojson = new \GeoJson\Feature\Feature($point, $properties);

            //add to array
            $geojsonFile->addMarker($geojson);
        }

        $geojsonFile->writeToFile(json_encode($geojson));

        $geojsonFile->closeFile();

        $this->flashMessage('Geojson súbor bol vytvorený');
    }

    protected function createComponentGeocachingEditForm() {
        $form = $this->geocachingEditFormFactory->create();

        $form->onSuccess[] = function($message) {
            $this->flashMessage($message, 'success');
            $this->presenter->redirect('this');
        };

        return $form;
    }

    protected function createComponentProcessGeocachingForm() {

        $form = $this->geocachingProcessFormFactory->create();

        $form->onSuccess[] = function($saved, $formValues) {
            if ($formValues['mapjson'] == TRUE) {
                $this->createGeojson();
            }
            $this->flashMessage('Do databázy boli úspešne vložené ' . $this->slovak->declension($saved, 'záznam', 'záznamy', 'záznamov'), 'success');
            $this->presenter->redirect('this');
        };

        $form->onError[] = function() {
            $this->flashMessage('Some items could not be imported, please check your import file.', 'error');
            $this->presenter->redirect('this');
        };

        return $form;
    }

    protected function createComponentGeocachingFilter() {
        $filter = $this->geocachingFilterFactory->create();

        return $filter;
    }

    public function handleFullForm($id) {

        $geocache = $this->geocachingManager->getCache($id);

        $this->setDefaults($geocache);

        $this->redrawControl();
    }

    private function setDefaults($geocache) {
        $array = [];

        $timestamp = $geocache->found->getTimestamp();

        $array['id'] = $geocache->id;
        $array['region'] = $geocache->region;
        $array['district'] = $geocache->district;
        $array['town'] = $geocache->town;
        $array['date'] = date('d.m.Y', $timestamp);
        $array['hour'] = date('H', $timestamp);
        $array['minutes'] = date('i', $timestamp);

        $this['geocachingEditForm']->setDefaults($array);
    }

    protected function createComponentGrid() {
        $grid = new \Grido\Grid();

        $basePath = $this->template->basePath;

        $repository = $this->em->getRepository('App\Geocaching');

        $query = $repository->createQueryBuilder('a') // We need to create query builder with inner join.
                ->addSelect('c')   // This will produce less SQL queries with prefetch.
                ->innerJoin('a.country', 'c')
                ->innerJoin('a.type', 'b')/* , array('country' => 'c.country', 'type' => 'b.name') */;

        $type = $this['geocachingFilter']->getType();
        $country = $this['geocachingFilter']->getCountry();
        $region = $this['geocachingFilter']->getRegion();
        $district = $this['geocachingFilter']->getDistrict();
        $town = $this['geocachingFilter']->getTown();

        if ($type) {
            $query
                    ->andWhere('a.type = :type')
                    ->setParameter('type', $type)
            ;
        }

        if ($country) {
            $query
                    ->andWhere('a.country = :country')
                    ->setParameter('country', $country)
            ;
        }

        if ($region && $region != 'Všetky') {
            $query
                    ->andWhere('a.region = :region')
                    ->setParameter('region', $region)
            ;
        }

        if ($district && $district != 'Všetky') {
            $query
                    ->andWhere('a.district = :district')
                    ->setParameter('district', $district)
            ;
        }

        if ($town && $town != 'Všetky') {
            $query
                    ->andWhere('a.town = :town')
                    ->setParameter('town', $town)
            ;
        }

        $model = new \Grido\DataSources\Doctrine($query, ['country' => 'c.country', 'type' => 'b.name']);

        $grid->model = $model;


        $grid->setDefaultSort(array('found' => 'desc'));
        $grid->setFilterRenderType(\Grido\Components\Filters\Filter::RENDER_INNER);

        $grid->addColumnText('id', 'ID')
                        ->setSortable()
                ->headerPrototype->style[] = "width:80px"
        ;

        $grid->addColumnText('country', 'K')
                ->setCustomRender(function($item) use ($basePath) {
                    $img = Html::el('img')->src("$basePath/images/flags/shiny/16/" . $item->country->image);
                    $img->title($item->country->country);
                    return $img;
                })
                ->setSortable()
                ->setColumn('country')
        //->setFilterSelect($this->getCountryFilter())
        ;

        $grid->getColumn('country')->headerPrototype->style[] = "width:20px";

        $grid->addColumnText('type', 'T')
                ->setCustomRender(function($item) use ($basePath) {
                    $img = Html::el('img')->src("$basePath/images/gcTypes/" . $item->type->image);
                    $img->title($item->type->name);
                    return $img;
                })
                ->setSortable()
        //->setFilterSelect($this->getTypeFilter())
        ;

        $grid->getColumn('type')->headerPrototype->style[] = "width:20px";

        $grid->addColumnText('name', 'Názov')
                        ->setSortable()
                ->headerPrototype->style[] = "width:455px"
        ;

        $grid->addColumnText('region', 'Región')
                        ->setSortable()
                ->headerPrototype->style[] = "width:140px"
        ;

        $grid->addColumnText('district', 'Okres')
                ->setSortable()
        ;

        $grid->getColumn('district')->headerPrototype->style[] = "width:140px";

        $grid->addColumnText('town', 'Mesto')
                ->setSortable()
        ;

        $grid->getColumn('town')->headerPrototype->style[] = "width:140px";

        $grid->addColumnText('altitude', 'Výška')
                        ->setSortable()
                ->cellPrototype->class[] = 'center'
        ;

        $grid->getColumn('altitude')->headerPrototype->style[] = "width:25px";


        $grid->addActionHref('edit', 'Edit')->setCustomRender(function($item) {
            $html = Html::el('span', [
                        'href' => $this->link('fullForm!', $id = $item->id),
                        'class' => 'gc-edit fa fa-pencil-square-o hand',
                        'data-toggle' => 'modal',
                        'data-target' => '#myModal',
                        'data-id' => $item->id
            ]);

            return $html;
        });


        return $grid;
    }

    public function actionDefault() {

        $params = $this['geocachingFilter']->params;

        $this['geocachingFilter']->setFilter($params);
    }

}
