<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\AdminModule\Presenters;

use Nette\Forms\Form;
use Nette\Image;
use Nette\Utils\Html;

class DiaryPresenter extends BasePresenter {

    /** @var \Components\Factories\IDiaryFormFactory @inject */
    public $diaryFormFactory;

    /** @var \Components\Factories\IStatsFilterFactory @inject */
    public $statsFilterFactory;

    /** @var \Components\Factories\IDiarySearchFormFactory @inject */
    public $diarySearchFormFactory;
    
    /** @var \Components\Factories\ITagsFactory @inject */
    public $tagsFactory;

    /** @var \App\Model\DiaryModel @inject */
    public $diaryModel;

    /** @var \App\Diary * */
    private $diary;

    /** @var string */
    private $search;

    public function __construct() {
        parent::__construct();
    }

    public function renderDefault() {
        $this->template->diaries = $this->diaryModel->getAll();
    }

    public function beforeRender() {
        parent::beforeRender();
        $this['breadcrumb']->addCrumb("Administrácia", ":Admin:Homepage:default");
        $this['breadcrumb']->addCrumb("Denník", ":Admin:Diary:default");

        $this['diarySearchForm']->setSearch($this->search, 'diary');
    }

    public function renderEdit() {
        $this->template->diaryForm = 'frm-' . $this['diaryForm']->name . '-form';
        $this['breadcrumb']->addCrumb($this->diary->title);
    }

    public function renderAdd() {
        $this->template->diaryForm = 'frm-' . $this['diaryForm']->name . '-form';
        $this['breadcrumb']->addCrumb("Nový denník");
    }

    public function actionEdit($id) {
        $this->diary = $this->em->find(\App\Diary::getClassName(), $id);
        $diary = $this->diary;

        if (!$diary) {
            $this->flashMessage("Denník neexisuje", "error");
            $this->redirect("default");
        }

        $this['diaryForm']->setMode(\Components\DiaryForm::EDIT_MODE);
        $this['diaryForm']->setDiary($diary);
    }

    public function handleDelete($id) {
        $this->diaryModel->delete($id);
        $this->redirect(":Admin:Diary:default");
    }

    protected function createComponentDiarySearchForm() {
        $form = $this->diarySearchFormFactory->create();

        $form->onResults[] = function($search) {
            $this->search = $search;
            if (!$this->isAjax()) {
                $this->redirect('this');
            } else {
                $this['grid']->redrawControl();
            }
        };

        return $form;
    }

    protected function createComponentGrid() {
        $grid = new \Grido\Grid();

        $from = $this['statsFilter']->getFrom();
        $to = $this['statsFilter']->getTo();

        $repository = $this->em->getRepository(\App\Diary::getClassName());

        $query = $repository->createQueryBuilder()
                ->addSelect('d')
                ->from(\App\Diary::getClassName(), 'd')
        ;

        if (isset($from)) {
            $query
                    ->where('d.fromDate >= :from')
                    ->andWhere('d.fromDate <= :to')
                    ->setParameter('from', $from)
                    ->setParameter('to', $to)
            ;
        }

        if (isset($this->search)) {
            $query
                    ->andWhere('d.title LIKE :search')
                    ->setParameter('search', '%' . $this->search . '%')
            ;
        }

        $model = new \Grido\DataSources\Doctrine($query);

        $grid->model = $model;

        $grid->setDefaultSort(['fromDate' => 'desc']);
        $grid->setFilterRenderType(\Grido\Components\Filters\Filter::RENDER_INNER);

        $grid->addColumnText('id', 'ID')
                        ->setSortable()
                ->cellPrototype->class[] = "center"
        ;

        $grid->getColumn('id')->headerPrototype->style[] = "width: 5%";

        $grid->addColumnText('title', 'Názov')
                        ->setSortable()
                ->headerPrototype->style[] = "width: 50%"
        ;

        $grid
                        ->addColumnDate('fromDate', 'Dátum', 'd.m.Y')
                        ->setCustomRender(function($diary) {
                            $ret = '';
                            $ret = $diary->fromDate->format('d.m.Y');

                            if (isset($diary->toDate)) {
                                $ret .= ' - ' . $diary->toDate->format('d.m.Y');
                            }

                            return Html::el('span')->addClass('grid')->addClass('admin')->addClass('date')->setText($ret);
                        })
                        ->setSortable()
                ->headerPrototype->style[] = "width: 15%"
        ;
        $grid->getColumn('fromDate')->cellPrototype->class[] = 'center';

        $grid->addActionHref('edit', 'Upraviť')
                ->setCustomRender(function($item, Html $el) {
                    ($el->attrs['class'][1] = NULL);
                    $el->addAttributes(['title' => 'Upraviť']);
                    $el->removeChildren();
                    $button = Html::el('span')->class('fa fa-edit');
                    $el->addHtml($button);
                    return $el;
                })
        ;

        $grid->addActionHref('delete', 'Delete')
                ->setCustomHref(function($item) {
                    return $this->link("delete!", $item->id);
                })
                ->setCustomRender(function($item, Html $el) {
                    ($el->attrs['class'][1] = NULL);
                    $el->addAttributes(['title' => 'Odstrániť']);
                    $el->removeChildren();
                    $button = Html::el('span')->class('fa fa-trash-o');
                    $el->addHtml($button);
                    return $el;
                });

        /* $grid->addActionHref('delete', 'Delete')
          ->setCustomRender(function($item, Html $el){
          ($el->attrs['class'][1] = NULL);
          $el->addAttributes(['title' => 'Odstrániť']);
          $el->removeChildren();
          $button = Html::el('span')->class('fa fa-trash-o');
          $el->addHtml($button);
          return $el;
          }); */
        //dump(__DIR__.'\..\templates\grido.latte');exit;
        //$grid->setTemplateFile(__DIR__.'\..\templates\grido.latte');
        
        //$grid->setPagination(TRUE);

        return $grid;
    }

    protected function createComponentStatsFilter() {
        $filter = $this->statsFilterFactory->create();
        $filter->setSessionNamespace('admin-diary');

        return $filter;
    }

    public function createComponentDiaryForm() {

        $form = $this->diaryFormFactory->create();

        if (isset($this->id)) {
            $form->setMode(\Components\DiaryForm::EDIT_MODE);
        } else {
            $form->setMode(\Components\DiaryForm::INSERT_MODE);
        }

        $form->onError[] = function($error) {
            $this->flashMessage('Some items could not be imported, please check your import file.', 'error');
            $this->presenter->redirect('this');
        };

        $form->onFileUploadError[] = function($error) {
            dump($error);exit;
            $this->flashMessage('Súbor s fotografiami sa nepodarilo nahrať: error: ' . $error, 'error');
            //$this->presenter->redirect('this');
        };

        $form->onDiarySaved[] = function($message, $diary = NULL) {
            if ($diary == NULL) {
                $this->flashMessage($message, 'ok');
                $this->presenter->redirect(':Admin:Diary:default');
            }
        };

        return $form;
    }

    private function getFileFromZip($ziparchive) {

        $array = array();

        for ($i = 0; $i < $ziparchive->numFiles; $i++) {
            $stat = $ziparchive->statIndex($i);
            //print_r( basename( $stat['name'] ) . PHP_EOL );

            $array[] = basename($stat['name']);
        }
        return $array;
    }

}
