<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HomePagePresenter
 *
 * @author Administrator
 */

namespace App\AdminModule\Presenters;

class HomepagePresenter extends BasePresenter {

    private $geocaching;

    public function createComponentGeocaching($geocaching) {
        $geocaching = new \App\Geocaching\Geocaching();

        return $geocaching;
    }

    public function renderDefault() {
        $cronTime = $this->getCronLastRunTime();
        $this->template->cronTime = $cronTime;
    }

}
