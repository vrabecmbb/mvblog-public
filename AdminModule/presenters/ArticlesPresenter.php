<?php

namespace App\AdminModule\Presenters;

use Nette\Forms\Form;
use Nette\Image;
use Nette\Utils\Html,
    \Dates\Dates;

class ArticlesPresenter extends BasePresenter {
    /*
     * @var \App\Model\ArticlesModel @inject
     */

    private $articlesModel;

    /* konkretny zapis pre editaciu */
    private $id;

    /** @var \App\Articles * */
    private $article;
    protected $filter;

    /** @var Dates @inject */
    public $dates;

    /** @var string */
    private $search;

    /** @var \Components\Factories\IArticleFormFactory @inject */
    public $articleFormFactory;

    /** @var \Components\Factories\IStatsFilterFactory @inject */
    public $statsFilterFactory;

    /** @var \Components\Factories\IDiarySearchFormFactory @inject */
    public $searchFactory;

    const FILTER_ALL = 'all';
    const FILTER_SAVED = 'saved';
    const FILTER_PUBLISHED = 'published';
    const FILTER_WAITING = 'waiting';

    public function __construct() {
        parent::__construct();
    }

    public function beforeRender() {
        parent::beforeRender();
        $this['breadcrumb']->addCrumb("Administrácia", ":Admin:Homepage:default");
        $this['breadcrumb']->addCrumb("Články", ":Admin:articles:default");

        $this['articleSearchForm']->setSearch($this->search, 'article');
    }

    protected function createComponentArticleSearchForm() {
        $form = $this->searchFactory->create();

        $form->onResults[] = function($search) {
            $this->search = $search;
            if (!$this->isAjax()) {
                $this->redirect('this');
            } else {
                $this['grid']->redrawControl();
            }
        };

        return $form;
    }

    public function renderDefault() {
        $this->template->filter = $this->filter;
    }

    public function renderAdd() {
        $this['breadcrumb']->addCrumb("Nový článok");
        $this->template->articleForm = 'frm-' . $this['articleForm']->name . '-form-btnSubmit';
    }

    public function renderEdit() {
        $this['breadcrumb']->addCrumb("Úprava");

        $this->template->articleForm = 'frm-' . $this['articleForm']->name . '-form-btnSubmit';
        $this->template->publishForm = 'frm-' . $this['articleForm']->name . '-form-btnPublish';

        $this->template->publishButton = $this['articleForm']->getMode() == \Components\ArticleForm::SAVED ? TRUE : FALSE;
    }

    public function actionDefault($filter) {
        $this->filter = $filter ? $filter : self::FILTER_ALL;
    }

    protected function createComponentGrid() {
        $grid = new \Grido\Grid();

        $from = $this['statsFilter']->getFrom();
        $to = $this['statsFilter']->getTo();

        $repository = $this->em->getRepository(\App\Articles::getClassName());

        $query = $repository->createQueryBuilder()
                ->addSelect('a')
                ->from(\App\Articles::getClassName(), 'a')
        ;

        if (isset($from)) {
            $query
                    ->andWhere('a.created >= :fromDate')
                    ->andWhere('a.created <= :toDate')
                    ->setParameter('fromDate', $from)
                    ->setParameter('toDate', $to)
            ;
        }

        if (isset($this->search)) {
            $query
                    ->andWhere('a.title LIKE :search')
                    ->setParameter('search', '%' . $this->search . '%')
            ;
        }

        $filter = $this->filter;

        $grid->setDefaultSort(['id' => 'desc']);
        $grid->setFilterRenderType(\Grido\Components\Filters\Filter::RENDER_INNER);

        $grid->addColumnText('id', 'ID')
                        ->setSortable()
                ->cellPrototype->class[] = "center"
        ;

        $now = new \DateTime;

        if ($filter == self::FILTER_SAVED) {
            $query->andWhere('a.published = FALSE');
        }
        if ($filter == self::FILTER_PUBLISHED) {
            $query
                    ->andWhere('a.published = TRUE')
            //->andWhere('a.accessDate <= :now')
            //->setParameter('now',$now)
            ;
        }
        if ($filter == self::FILTER_WAITING) {
            $query
                    ->andWhere('a.published = TRUE')
                    ->andWhere('a.accessDate > :now')
                    ->setParameter('now', $now)
            ;
        }

        if (isset($this->search)) {
            $query
                    ->andWhere('a.title LIKE :search')
                    ->setParameter('search', '%' . $this->search . '%')
            ;
        }

        $model = new \Grido\DataSources\Doctrine($query);

        $grid->model = $model;

        $grid->getColumn('id')->headerPrototype->style[] = "width: 5%";

        $grid->addColumnText('title', 'Názov')
                        ->setSortable()
                ->headerPrototype->style[] = "width: 50%"
        ;

        $grid
                        ->addColumnDate('created', 'Vytvorené', 'd. m. Y')
                ->headerPrototype->style[] = "width: 15%"
        ;

        $grid
                ->addColumnText('published', 'Stav')
                ->setCustomRender(function($item) {

                    $now = new \DateTime;

                    if (!$item->published) {
                        $status = Html::el('span')->class('status green')->setText('Uložené');
                        $date = $item->modified ? $item->modified : $item->created;
                    }

                    if ($item->published && $item->accessDate <= $now) {
                        $status = Html::el('span')->class('status blue')->setText('Publikované');
                        $date = $item->accessDate;
                    }
                    if ($item->published && $item->accessDate >= $now) {
                        $status = Html::el('span')->class('status red')->setText('Čakajúce');
                        $date = $item->accessDate;
                    }

                    $spanDate = Html::el('span')->addClass('date')->setText($this->dates->toFormatedString($date, Dates::SK));

                    $div = Html::el('div')->addClass('status');

                    $div->add($status);
                    $div->add(Html::el('br'));
                    $div->add($spanDate);

                    return $div->__toString();
                })
        ;

        $grid->addActionHref('edit', 'Upraviť')
                ->setCustomRender(function($item, Html $el) {
                    ($el->attrs['class'][1] = NULL);
                    $el->addAttributes(['title' => 'Upraviť']);
                    $el->removeChildren();
                    $button = Html::el('span')->class('fa fa-edit');
                    $el->add($button);
                    return $el;
                })
        ;

        $grid->addActionHref('delete', 'Delete')
                ->setCustomRender(function($item, Html $el) {
                    ($el->attrs['class'][1] = NULL);
                    $el->addAttributes(['title' => 'Odstrániť']);
                    $el->removeChildren();
                    $button = Html::el('span')->class('fa fa-trash-o');
                    $el->add($button);
                    return $el;
                })
        ;
        //dump(__DIR__.'\..\templates\grido.latte');exit;		
        //$grid->setTemplateFile(__DIR__.'\..\templates\grido.latte');

        return $grid;
    }

    protected function createComponentStatsFilter() {
        $filter = $this->statsFilterFactory->create();
        $filter->setSessionNamespace('admin-article');

        return $filter;
    }

    public function actionAdd() {
        $this['articleForm']->setMode(\Components\ArticleForm::INSERT_MODE);
    }

    public function actionEdit($id) {
        $this->id = $id;

        $this->article = $this->em->find(\App\Articles::getClassName(), $id);
        $article = $this->article;

        if (!$article) {
            $this->flashMessage("Článok neexisuje", "error");
            $this->redirect("default");
        }

        $article->published ?
                        $this['articleForm']->setMode(\Components\ArticleForm::EDIT_MODE) :
                        $this['articleForm']->setMode(\Components\ArticleForm::SAVED);

        $this['articleForm']->setArticle($article);
    }

    public function handleDelete($id) {
        $this->articlesModel->delete($id);
        $this->redirect(":Admin:Articles:default");
    }

    public function createComponentArticleForm() {
        $form = $this->articleFormFactory->create();

        $form->onArticleSaved[] = function($message, $article) {
            $this->flashMessage($message);
            $id = $article->id;
            $this->redirect(":Admin:Articles:edit", $id);
        };

        $form->onArticlePublished[] = function() {
            $this->flashMessage('Článok bol publikovaný', 'ok');
            $this->redirect(":Admin:Articles:default");
        };

        $form->onError[] = function($error) {
            $this->flashMessage('Some items could not be imported, please check your import file.', 'error');
            //$this->presenter->redirect('this');
        };

        return $form;
    }

}
