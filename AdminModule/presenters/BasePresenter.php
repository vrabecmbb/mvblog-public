<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BasePresenter
 *
 * @author Administrator
 */

namespace App\AdminModule\Presenters;

use Nette\Forms\Container;

class BasePresenter extends \App\Presenters\ParentPresenter {

    /** @var \Doctrine\ORM\EntityManager @inject */
    public $em;

    public function startup() {
        parent::startup();

        if (!$this->user->loggedIn)
            $this->redirect(':Front:Homepage:Auth');
    }

    public function __construct() {
        Container::extensionMethod('addDatePicker', function ($container, $name, $label = NULL) {
            return $container[$name] = new \JanTvrdik\Components\DatePicker($label);
        });
    }

    public function createComponentFlashes() {
        $flashes = new \components\FlashMessages();

        return $flashes;
    }

    public function getCronLastRunTime() {
        $file = file_get_contents('nette.safe://crontime.txt');

        $date = new \DateTime;
        $date->setTimestamp($file);

        return $date->format('d.m.Y H:i:s');
    }

}
