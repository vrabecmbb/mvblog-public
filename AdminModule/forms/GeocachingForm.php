<?php

use Nette\
    Nette\Forms\Form;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AdminModule\Forms;
/**
 * Description of loginForm
 *
 * @author Administrator
 */
class GeocachingForm extends \Nette\Application\UI\Control
{
    /** @persistent */
    public $backlink;
    
    public function render()
    {
        $this->template->setFile(__DIR__.'/GeocachingForm.latte');
        $this->template->render();
    }
    
    protected function createComponentLogin() 
    {
        $form = new \Nette\Application\UI\Form();
        $form->addText('login','Login:')
                ->setRequired();
        $form->addPassword('password', 'Heslo:')
                ->setRequired();
        $form->addSubmit('unlockAdministration');
        
        $form->onSuccess[] = callback($this, 'processLogin');
        
        return $form;
    }
    
    public function processLogin(\Nette\Application\UI\Form $form)
    {
        $values = $form->getValues();
        
    }
    
    /*public function __construct(\Nette\IComponentContainer $parent=NULL, $name=NULL) {
        parent::__construct($parent, $name);
        
        $this->onSuccess[] = array($this, 'processSuccess');

        $this->addText('email')
            ->addRule(Form::FILLED, 'Enter login email')
            ->addRule(Form::EMAIL, 'Filled value is not valid email');

        $this->addPassword('password')
            ->addRule(Form::FILLED, 'Enter password');

        $this->addCheckbox('remember');

        $this->addSubmit('send', 'Log in!');
    }


    public function processSuccess(AppForm $form)
    {
        $user = $this->presenter->user;

        if (TRUE === $form->values['remember']) {
            $user->setExpiration('+30 days', $whenBrowserIsClosed = FALSE);
        } else {
            $user->setExpiration(0, $whenBrowserIsClosed = TRUE);
        }

        try {
            $user->login($form['email']->value, $form['password']->value);
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
            return;
        }

        $this->presenter->application->restoreRequest($this->presenter->backlink);
        $this->presenter->redirect('Default:default');
    }*/
}
