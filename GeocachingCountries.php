<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Geocaching_countries
 *
 * @author Administrator
 * @ORM\Entity
 * @ORM\Table(name="geocaching_countries")
 */
class GeocachingCountries extends \Kdyby\Doctrine\Entities\BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="Geocaching", mappedBy="country")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column (type="string")
     */
    public $country;

    /**
     * @ORM\Column (type="string")
     */
    public $originalName;

    /**
     * @ORM\Column (type="string") 
     */
    public $image;

}
