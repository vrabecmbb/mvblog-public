<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\FrontModule\Presenters;

use Nette\Utils\Html;

class GeocachingPresenter extends BasePresenter {

    /** @var \Kdyby\Doctrine\EntityManager @inject */
    public $database;

    /** @var \GeocachingManager @inject */
    public $geocachingManager;

    /** @var \GeocachingStatsManager @inject */
    public $geocachingStatsManager;

    /** @var \Components\Factories\IGeocachingGlobalStatsFactory @inject */
    public $geocachingGlobalStatsFactory;

    /** @var \Components\Factories\IGeocachingRankingFactory @inject */
    public $geocachingRankingFactory;

    /** @var \Helpers\Locations @inject */
    public $locations;

    /** @var \Components\Factories\IGeocachingFilterFactory @inject */
    public $geocachingFilterFactory;

    /** @var \Components\Factories\IStatsFilterFactory @inject */
    public $statsFilterFactory;

    public function beforeRender() {
        parent::beforeRender();
        $this->template->geocachingManager = $this->geocachingManager;
    }

    protected function createComponentGeocachingGlobalStats() {
        $globalStats = $this->geocachingGlobalStatsFactory->create();

        return $globalStats;
    }

    protected function createComponentGeocachingFilter() {
        $filter = $this->geocachingFilterFactory->create();

        return $filter;
    }

    public function actionDefault() {

        $params = $this['geocachingFilter']->params;

        $this['geocachingFilter']->setFilter($params);
    }

    protected function createComponentGrid() {
        $grid = new \Grido\Grid();

        $basePath = $this->template->basePath;

        $repository = $this->em->getRepository('App\Geocaching');

        $from = $this['statsFilter']->getFrom();
        $to = $this['statsFilter']->getTo();

        $query = $repository->createQueryBuilder('a') // We need to create query builder with inner join.
                ->addSelect('c')   // This will produce less SQL queries with prefetch.
                ->innerJoin('a.country', 'c')
                ->innerJoin('a.type', 'b')/* , array('country' => 'c.country', 'type' => 'b.name') */;

        $type = $this['geocachingFilter']->getType();
        $country = $this['geocachingFilter']->getCountry();
        $region = $this['geocachingFilter']->getRegion();
        $district = $this['geocachingFilter']->getDistrict();
        $town = $this['geocachingFilter']->getTown();

        if ($type) {
            $query
                    ->andWhere('a.type = :type')
                    ->setParameter('type', $type)
            ;
        }

        if ($country) {
            $query
                    ->andWhere('a.country = :country')
                    ->setParameter('country', $country)
            ;
        }

        if ($region && $region != 'Všetky') {
            $query
                    ->andWhere('a.region = :region')
                    ->setParameter('region', $region)
            ;
        }

        if ($district && $district != 'Všetky') {
            $query
                    ->andWhere('a.district = :district')
                    ->setParameter('district', $district)
            ;
        }

        if ($town && $town != 'Všetky') {
            $query
                    ->andWhere('a.town = :town')
                    ->setParameter('town', $town)
            ;
        }

        //statsfilter
        if (isset($from)) {
            $query
                    ->andWhere('a.found >= :from')
                    ->andWhere('a.found <= :to')
                    ->setParameter('from', $from)
                    ->setParameter('to', $to)
            ;
        }


        $model = new \Grido\DataSources\Doctrine($query, ['country' => 'c.country', 'type' => 'b.name']);

        $grid->model = $model;


        $grid->setDefaultSort(array('found' => 'desc'));
        $grid->setFilterRenderType(\Grido\Components\Filters\Filter::RENDER_INNER);

        $grid->addColumnText('id', 'ID')
                        ->setSortable()
                ->headerPrototype->style[] = "width:80px"
        ;

        $grid->addColumnText('country', 'K')
                ->setCustomRender(function($item) use ($basePath) {
                    $img = Html::el('img')->src("$basePath/images/flags/shiny/16/" . $item->country->image);
                    $img->title($item->country->country);
                    return $img;
                })
                ->setSortable()
                ->setColumn('country')
        //->setFilterSelect($this->getCountryFilter())
        ;

        $grid->getColumn('country')->headerPrototype->style[] = "width:20px";

        $grid->addColumnText('type', 'T')
                ->setCustomRender(function($item) use ($basePath) {
                    $img = Html::el('img')->src("$basePath/images/gcTypes/" . $item->type->image);
                    $img->title($item->type->name);
                    return $img;
                })
                ->setSortable()
        //->setFilterSelect($this->getTypeFilter())
        ;

        $grid->getColumn('type')->headerPrototype->style[] = "width:20px";

        $grid->addColumnText('name', 'Názov')
                        ->setSortable()
                ->headerPrototype->style[] = "width:455px"
        ;

        $grid->addColumnText('region', 'Región')
                        ->setSortable()
                ->headerPrototype->style[] = "width:140px"
        ;

        $grid->addColumnText('district', 'Okres')
                ->setSortable()
        ;

        $grid->getColumn('district')->headerPrototype->style[] = "width:140px";

        $grid->addColumnText('town', 'Mesto')
                ->setSortable()
        ;

        $grid->getColumn('town')->headerPrototype->style[] = "width:140px";

        $grid->addColumnText('altitude', 'Výška')
                        ->setSortable()
                ->cellPrototype->class[] = 'center'
        ;

        $grid->getColumn('altitude')->headerPrototype->style[] = "width:25px";

        return $grid;
    }

    protected function createComponentStatsFilter() {
        $filter = $this->statsFilterFactory->create();
        $filter->setSessionNamespace('front-geocaching');

        return $filter;
    }

    protected function createComponentGeocachingRanking() {
        $ranking = $this->geocachingRankingFactory->create();

        $ranking->setEm($this->em);

        return $ranking;
    }

    protected function createComponentMapBox() {
        return new \components\MapBox();
    }

}
