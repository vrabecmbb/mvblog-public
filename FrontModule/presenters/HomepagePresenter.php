<?php

namespace App\FrontModule\Presenters;

use Nette;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter {
	
	private $diaries;
	
	private $articles;
	
	private $geocaches;
	
	/** @var \App\Model\ArticleModel @inject */
	public $articleModel;

	/** @var \App\Model\DiaryModel @inject */
	public $diaryModel;
	
	/** @var \GeocachingManager @inject */
	public $geocachingManager;
	
	/** @var \Components\Factories\IFlickrFactory @inject */
	public $flickrFactory;

	/** @var \Doctrine\ORM\EntityManager @inject */
	public $em;

	/**
	 * actionAuth (autentifikacia administracia)
	 */
	public function createComponentAdmin() {
		$form = new Nette\Application\UI\Form();

		$form->addText('login', 'Login:')
				->setRequired()
				->setAttribute('class', 'input-xlarge');
		$form->addPassword('heslo', 'Heslo:')
				->setRequired()
				->setAttribute('class', 'input-xlarge');
		$form->addSubmit('ok', 'Vstup do administrácie');

		$form->onSuccess[] = [$this, 'processAdministration'];

		return $form;
	}

	public function processAdministration(Nette\Application\UI\Form $form) {
		$login = $form->getValues()['login'];
		$password = $form->getValues()['heslo'];

		try {
			$this->user->login($login, $password);
			$response = $this->getHttpResponse();
			$response->setCookie('nette-debug', 'mytopsecret', strtotime('1 week'));
			$response->setCookie('admin-mode', (bool)true, strtotime('1 week'));
			$this->redirect(':Admin:Homepage:default');
			
		} catch (\Nette\Security\AuthenticationException $ex) {
			//$form->addError('Vstup do administrácie zamietnutý');
			$form->addError($ex->getMessage());
		}
	}
	
	public function actionLogout() {
		
		$this->user->logout();
	}
	
	public function createComponentFlickr() {
		$flickr = $this->flickrFactory->create();

        return $flickr;
	}

	public function actionDefault() {
		$this->diaries = $this->diaryModel->getLimited(5);
		$this->geocaches = $this->geocachingManager->getNewest(8);
		$this->articles = $this->articleModel->getLimited(6);
	}

	public function renderDefault() {
		$this->template->diaries = $this->diaries;
		$this->template->articles = $this->articles;
		$this->template->geocaches = $this->geocaches;
		
		//$this->template->panoramio_script = $this['panoramio']->getUrl();
	}

}
