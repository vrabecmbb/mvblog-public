<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\FrontModule\Presenters;

/**
 * Description of DiariesPresenter
 *
 * @author Administrator
 */
class DiariesPresenter extends \App\FrontModule\Presenters\BasePresenter {

    private $months = [1 => 'Január', 'Február', 'Marec', 'Apríl', 'Máj', 'Jún', 'Júl', 'August', 'September', 'Október', 'November', 'December'];

    /** @var int */
    private $month;

    /** @var int */
    private $year;

    /** @var App\Diary */
    private $diary;

    /** @var \App\Model\DiaryModel @inject */
    public $diaryModel;

    public function beforeRender() {
        parent::beforeRender();
        $this['breadcrumb']->addCrumb("Denník", "Diaries:default");
        if ($this->diary) {
            $this['breadcrumb']->addCrumb($this->diary->title);
        }
    }

    public function actionDefault($month, $year) {

        $this->month = $month;
        $this->year = $year;

        if ($month == NULL || $year == NULL) {
            if (!$this->diaryModel->getByTime($month, $year)) {
                $month = NULL;
                $year = NULL;
            }
        }

        //rok a mesiac najstarsieho zapisu v dennicku
        $oldest_time = ($this->getTime($this->diaryModel->getOldest()->fromDate->getTimestamp()));
        $this->template->oldest_time = $oldest_time;

        $newest_time = ($this->getTime($this->diaryModel->getNewest()->fromDate->getTimestamp()));
        $this->template->newest_time = $newest_time;

        if (is_null($month) && is_null($year)) {
            $this->month = $newest_time['month'];
            $this->year = $newest_time['year'];
        }

        if (!@checkdate($this->month, 1, $this->year)) {
            $this->redirect('default');
        }

        //Month
        $nextMonth = $this->diaryModel->getNextTime($month, $year);
        $previousMonth = $this->diaryModel->getPreviousTime($month, $year);

        //Year
        $nextYear = $this->diaryModel->getNextTime(1, $year + 1);
        $previousYear = $this->diaryModel->getPreviousTime(12, $year - 1);
    }

    public function renderDefault() {

        $month = $this->month;
        $year = $this->year;

        //Nepresiahnutie maxima a minima
        if ($this->diaryModel->getByTime($month, $year) === FALSE){
            if ($month != 1) {
                $this->redirect("Diaries:default", array($month = ($month - 1), $year));
            } else {
                $this->redirect("Diaries:default", array($month = 12, $year = ($year - 1)));
            }
        }


        //Krokovanie zo sablony
        $this->template->nextMonth = $this->diaryModel->getNextTime($month, $year);
        $this->template->previousMonth = $this->diaryModel->getPreviousTime($month, $year);

        $this->template->nextYear = $this->diaryModel->getNextTime(1, $year + 1);
        $this->template->previousYear = $this->diaryModel->getPreviousTime(12, $year - 1);

        $this->template->diaries = $this->diaryModel->getByTime($month, $year);

        $this->template->year = $year;
        $this->template->month = $month;

        $this->template->monthLabel = $this->months[(int) $month];
        $this->template->yearLabel = $year;
    }

    public function actionShow($id) {

        //get diary
        $diary = $this->diaryModel->getDiary((int) $id);

        if (is_null($diary)) {
            $this->redirect('default');
        }

        $this->diary = $diary;
    }

    public function renderShow() {
        $this->template->diary = $this->diary;
    }

    public static function getTime($timestamp) {
        $toRet = array(
            'month' => (int) date('m', $timestamp),
            'year' => (int) date('Y', $timestamp)
        );

        return $toRet;
    }

}
