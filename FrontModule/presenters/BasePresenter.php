<?php

namespace App\FrontModule\Presenters;

use Nette,
	App\Model,
	Nette\Forms\FormContainer;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\Presenters\ParentPresenter {
	/* @var \App\Model\GeocachingModel */

	public $adminMode;

	/** @var \Doctrine\ORM\EntityManager @inject */
	public $em;

	/** @var \Components\Factories\INavigationFactory @inject */
	public $navigationFactory;

	/** @var \Components\Factories\IMailFormFactory @inject */
	public $mailFormFactory;

	public function beforeRender() {
		parent::beforeRender();
		$this->template->adminMode = $this->adminMode;
	}

	public function injectAdminMode(){
		$this->adminMode = (bool)$this->getHttpRequest()->getCookie('admin-mode');
	}

	public function handleChangeAdminMode(){
		$adminMode = $this->adminMode;

		if ($adminMode == true){
			$this->getHttpResponse()->setCookie('admin-mode', FALSE, strtotime('1 week'));
		}else {
			$this->getHttpResponse()->setCookie('admin-mode', TRUE, strtotime('1 week'));
		}

		$this->redirect('this');
	}

	public function handleLogout(){
		$this->user->logout();
		$this->redirect("this");
	}

	public function createComponentGeocaching() {
		$model = $this->geocachingModel;
		$component = new \App\Geocaching\Geocaching($model);

		return $component;
	}

	public function createComponentNavigation() {
		$navigation = $this->navigationFactory->create();

		return $navigation;
	}

	public function createComponentMailForm(){
		$mailform = $this->mailFormFactory->create();
        
         $mailform->onSuccess[] = function($message) {
            $this->flashMessage($message, 'success');
            $this->redirect('this');
        };

		return $mailform;
	}

}
