<?php

namespace App\FrontModule\Presenters;

/**
 * Description of ArticlesPresenter
 *
 * @author Administrator
 */
class ArticlesPresenter extends BasePresenter {

	private $article;

	/** @var \Components\Factories\ICalendarFactory @inject **/
	public $calendarFactory;

	/** @var \App\Model\ArticleModel @inject * */
	public $articlesModel;

	public function actionShow($id) {

		if ($this->user->isInRole('admin') && $this->adminMode) {
			$article = $this->articlesModel->getAdminArticle($id);
		} else {
			$article = $this->articlesModel->getPublishedArticle($id);
		}

		if (empty($article)) {
			$this->redirect("Articles:default");
		}

		$this->article = is_array($article) ? $article[0] : $article;

	}

	protected function createComponentCalendar(){
		$cal = $this->calendarFactory->create();

		$cal->setCalendarGenerator(
			new \CalendarGenerator(new \IHorizontalCalendarCellFactory(1))
		);

		//$cal->truncateDaysLabelsTo(2);
		return $cal;
	}

	public function renderShow() {

		$this->template->article = $this->article;
	}

	public function renderDefault() {
		if ($this->adminMode) {
			$this->template->articles = $this->articlesModel->getAllArticles();
		} else {
			$this->template->articles = $this->articlesModel->getPublishedArticles();
		}
	}
}
