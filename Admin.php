<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Admin extends \Kdyby\Doctrine\Entities\BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $admin_id;
    
    /**
     * @ORM\Column(type="string")
     */
    private $role;
    
    /**
     * @ORM\Column(type="string")
     */
    private $login;
    
    /**
     * @ORM\Column(type="string")
     */
    private $password;
    
    /**
     * @return int
     */
    public function getId()
    {
	return $this->admin_id;
    }
    
    public function getRole()
    {
        return $this->role;
    }
	
    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->login;
    }
	
    /**
     * @return string
     */
    public function getPassword()
    {
	return $this->password;
    }
    
    public function setPassword($password)
    {
        $this->password = $password;
    }
    
    public function setUsername($username)
    {
        $this->login = $username;
    }
    
    public function setRole($role)
    {
        $this->role = $role;
    }

}
