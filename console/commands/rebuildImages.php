<?php

namespace Console\Command;


use Symfony\Component\Console;


class RebuildImages extends Console\Command\Command
{

	/** @var \App\Model\DiaryModel */
	private $diaryModel;

	/** @var \Doctrine\ORM\EntityManager */
	private $em;

	function __construct(\App\Model\DiaryModel $diaryModel, \Doctrine\ORM\EntityManager $em) {
		parent::__construct();
		$this->diaryModel = $diaryModel;
		$this->em = $em;
	}

	
	protected function configure()
	{
		$this
				->setName('image:reinit')
				->setDescription('Reinit images path in diaries')
		;
	}


	protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
	{
		$progress = new Console\Helper\ProgressHelper;
		$progress->setFormat(Console\Helper\ProgressHelper::FORMAT_VERBOSE);
		
		$diaries = $this->diaryModel->getAll();
		$progress->start($output, count($diaries));

		foreach($diaries as $diary){
			$photogallery = ltrim($diary->photogallery,'.');
			$photogallery = unserialize($photogallery);
			$galar = [];
			foreach($photogallery as $photo){
				$photo['image'] = str_replace("/MVtym/sandbox/www/uploads/","/uploads/diaries/",$photo["image"]);
				$photo['thumb'] = str_replace("/MVtym/sandbox/www/uploads/","/uploads/diaries/",$photo["thumb"]);
				array_push($galar, $photo);
			}
			$diary->photogallery = serialize($galar);
			$progress->advance();
		}
		
		
		$this->em->flush();
		$progress->finish();
	}


}
