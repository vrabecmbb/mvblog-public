<?php

require __DIR__ . '/../vendor/autoload.php';
use Tracy\Debugger;

//Config Dirs
define("LIBS_DIR", __DIR__ . "\..\libs");
define("LOG_DIR", __DIR__ . "/log");
define("TEMP_DIR", __DIR__ . "/temp");
define("APP_DIR", __DIR__);
//define("WWW_DIR", __DIR__ . "/www");

if (Nette\Environment::isConsole()) {
	//define("WWW_DIR", 'mvtym.local');
  define("WWW_DIR", 'mvonline.eu');
} else {
	define("WWW_DIR", $_SERVER['HTTP_HOST']);
}
$configurator = new Nette\Configurator;

$configurator->setDebugMode(TRUE);
//$configurator->setDebugMode('mytopsecret@188.123.100.95');  // 195.78.45.138debug mode MUST NOT be enabled on production server
$configurator->enableDebugger(__DIR__ . '/../log');

Debugger::$maxDepth = 5; // default: 3
Debugger::$maxLen = 200; // default: 150

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
		->addDirectory(__DIR__)
		->addDirectory(__DIR__ . '/../libs')
		->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');


if (php_sapi_name() == "cli" || isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] === "mvonline.eu"){
$configurator->addConfig(__DIR__ . '/config/config.mvonline.neon');
}

//$container = $configurator->createContainer();

return $configurator;
