<?php

namespace App\Presenters;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

/**
 * Description of ParentPresenter
 *
 * @author Administrator
 */
class ParentPresenter extends \Nette\Application\UI\Presenter{   
    
    /** @var \Nette\DI\Container @inject */
    public $container;
      
    public function beforeRender() {
        parent::beforeRender();
        $this->template->presenter = $this->presenter->name;
		$this->template->logoSmallImage = $this->context->parameters['logoSmallImage'];
        $this->template->logoImage = $this->context->parameters['logoImage'];
		$this->template->adminLogoImage = $this->context->parameters['adminLogoImage'];
    }
	
	public function createComponentBreadcrumb(){
		$breadcrumb = new \Components\Breadcrumb();
		
		return $breadcrumb;
	}
	
    public function getHttpQuery($value = NULL)
    {
        if ($value == NULL) {
            return $this->getHttpRequest()->getQuery();
        }
          return $this->getHttpRequest()->getQuery($value);  
    }
	
	public function getPureName()
    {
        $pos = strrpos($this->name, ':');
        if (is_int($pos)) {
            return substr($this->name, $pos + 1);
        }

        return $this->name;
    }
}
