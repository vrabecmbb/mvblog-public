<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * All entity classes must be declared as such.
 *
 * @ORM\Entity
 * @ORM\Table(name="diary")
 */
class Diary extends \Kdyby\Doctrine\Entities\BaseEntity{
    
    public function __construct() {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;
    
    /**
     * @ORM\Column(name="title")
     */
    public $title;
    
    /**
     * @ORM\Column (type="datetime") 
     */
    public $fromDate;
    
    /**
     * @ORM\Column (type="datetime", nullable=true) 
     */
    public $toDate;
	
	/**
     * @ORM\Column (type="text",) 
     */
    public $text;
    
    /**
     * @ORM\Column (type="text", nullable=true)
     */
    public $photogallery;
    
    /**
     * @ORM\ManyToMany(targetEntity="Tags", mappedBy="diaries", fetch="EXTRA_LAZY",cascade={"persist"})
     */
    public $tags;
    
    public function addTag(Tags $tag)
    {
        $this->tags->add($tag);
	$tag->addDiary($this);

	return $this;
    }
    
    public function getTags(){
        return $this->tags;
    }
    
    public function removeTag(Tags $tag)
    {
        $this->tags->removeElement($tag);
    }

}
    