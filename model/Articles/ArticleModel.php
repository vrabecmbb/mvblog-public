<?php

/**
 * Description of DiaryModel
 *
 * @author Administrator
 */

namespace App\Model;

use Nette,
	Nette\Utils\Strings;

class ArticleModel {

	private $em;
	private $diaryDao;
	private $now;

	public function __construct(\Kdyby\Doctrine\EntityManager $em) {
		$this->em = $em;
		$this->diaryDao = $em->getDao(\App\Articles::getClassName());
		$this->now = new \DateTime;
	}

	public function getAll() {
		return $this->em->getRepository(\App\Articles::getClassName())->findAll();
	}

	/*
	 * Vrati konkretny clanok
	 */

	public function getArticle($id) {
		return $this->em->find('\App\Articles', $id);
	}

	public function getPublishedArticles() {
		$qb = $this->em->createQueryBuilder()
				->select('a')
				->from(\App\Articles::getClassName(), 'a')
				->where('a.published = TRUE')
				->andWhere('a.accessDate <= :now')
				->orderBy('a.publishedDate', 'DESC')
				->setParameter('now', $this->now)
		;

		return $qb->getQuery()->getResult();
	}
	
	public function getAllArticles() {
		$qb = $this->em->createQueryBuilder()
				->select('a')
				->from(\App\Articles::getClassName(), 'a')
				->addOrderBy('a.created', 'DESC')
				->addOrderBy('a.publishedDate', 'DESC')
		;

		return $qb->getQuery()->getResult();
	}
	
	public function getByDate($date) {

		$start = $date;
		$start->setTime(0, 0, 0);
		$end = clone $start;
		$end->setTime(23, 59, 59);
		
		$qb = $this->em->createQueryBuilder();
		$qb
				->select('a')
				->from(\App\Articles::getClassName(), 'a')
				->addOrderBy('a.created', 'DESC')
				->where('a.publishedDate BETWEEN :start AND :end')
				->andWhere('a.accessDate <= :now')
				->addOrderBy('a.publishedDate', 'DESC')
				->setParameters(["start" => $start, "end" => $end, "now" => $this->now])
		;

		return $qb->getQuery()->getArrayResult();
	}
	
	public function getByMonth($month, $year) {

		$start = new \DateTime;
		$start->setDate($year, $month, 1);
		$start->setTime(0, 0, 0);
		$end = clone $start;
		$end_day = date("t", mktime(23, 59, 59, $month, 1, $year));
		$end->setDate($year, $month, $end_day);
		$end->setTime(23, 59, 59);
		
		$qb = $this->em->createQueryBuilder();
		$qb
				->select('a')
				->from(\App\Articles::getClassName(), 'a')
				->addOrderBy('a.created', 'DESC')
				->where('a.publishedDate BETWEEN :start AND :end')
				->andWhere('a.accessDate <= :now')
				->addOrderBy('a.publishedDate', 'DESC')
				->setParameters(["start" => $start, "end" => $end, "now" => $this->now])
		;

		return $qb->getQuery()->getArrayResult();
	}

	public function getPublishedArticle($id) {
		$now = new \DateTime;
		$qb = $this->em->createQueryBuilder();

		$qb->addSelect('a')
				->from(\App\Articles::getClassName(), 'a')
				->andWhere('a.published = TRUE')
				->andWhere('a.id = :id')
				->andWhere('a.accessDate <= :now')
				->setParameters(['id' => $id, 'now' => $this->now])
		;
		
		$result = $qb->getQuery()->getResult();

		return $result;
	}
	
	public function getAdminArticle($id) {
		$qb = $this->em->createQueryBuilder();

		$qb->addSelect('a')
				->from(\App\Articles::getClassName(), 'a')
				->andWhere('a.created IS NOT NULL')
				->andWhere('a.id = :id')
				->setParameter('id', $id)
		;
		
		$result = $qb->getQuery()->getSingleResult();

		return $result;
	}

	public function getLimited($limit) {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('article')
				->from(\App\Articles::getClassName(), 'article')
				->where('article.accessDate <= :now')
				->andWhere('article.published = TRUE')
				->setMaxResults($limit)
				->orderBy('article.created', 'DESC')
				->setParameter('now', $this->now)
		;

		return $qb->getQuery()->getResult();
	}

	/**
	 * Aktualizacia clanku
	 * 
	 * @param type $article
	 */
	public function update($article) {

		$article->modified = new \DateTimeImmutable;
		$this->em->flush();
		//$this->getTable()->where('id=?', $id)->update($data);
		//$article->
	}

	/**
	 * Vydanie clanku
	 * 
	 * @param type $article
	 */
	public function publish($article) {

		$date = new \DateTimeImmutable;

		$article->published = TRUE;
		$article->publishedDate = $date;
		$article->modified = $date;
		$this->em->flush();
	}

	/**
	 * Vlozenie noveho prispecku do dennicka
	 * @param type $data
	 * @return type
	 */
	public function insert($article) {
		$now = new \DateTimeImmutable;

		$article->created = $now;
		$article->published = FALSE;

		$this->em->persist($article);
		$this->em->flush();

		return $article->id;
	}

	public function saveIntroImage(\App\Articles $article, $image) {
		$article->introPhoto = $image;

		$this->em->persist($article);
		$this->em->flush();

		return $article;
	}

	public function delete($id) {
		$this->getTable()->where('id', $id)->delete($id);
	}

	/**
	 * Vrati najnovsi clanok
	 * @return type
	 */
	public function getNewest() {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('article')
				->from(\App\Articles::getClassName(), 'article')
				->setMaxResults(1)
				->orderBy('article.created', 'DESC')
		;

		return $qb->getQuery()->getResult()[0];
	}

	public function removeIntro($article) {
		$article->introPhoto = NULL;
		$this->em->flush();
	}

}
