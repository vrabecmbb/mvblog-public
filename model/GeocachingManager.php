<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authenticator
 *
 * @author Administrator
 */
class GeocachingManager {

	private $em;
	private $geocachingDao;

	public function __construct(Kdyby\Doctrine\EntityManager $em) {
		$this->em = $em;
		$this->geocachingDao = $em->getDao(\App\Geocaching::getClassName());
	}

	public function getAll() {
		return $this->geocachingDao->findAll();
	}

	public function update() {
		$this->em->flush();
	}
	
	public function getByDate($date) {

		$start = $date;
		$start->setTime(0, 0, 0);
		$end = clone $start;
		$end->setTime(23, 59, 59);
		
		$qb = $this->em->createQueryBuilder();
		$qb
				->select(['gc', 'c', 't'])
				->from(\App\Geocaching::getClassName(), 'gc')
				->leftJoin('gc.country', 'c')
				->leftJoin('gc.type', 't')
				->where('gc.found BETWEEN :start AND :end')
				->orderBy('gc.found', 'DESC')
				->setParameters(["start" => $start, "end" => $end])
		;

		return $qb->getQuery()->getArrayResult();
	}

	public function getByFoundMonth($month, $year) {

		$start = new DateTime;
		$start->setDate($year, $month, 1);
		$start->setTime(0, 0, 0);
		$end = clone $start;
		$end_day = date("t", mktime(23, 59, 59, $month, 1, $year));
		$end->setDate($year, $month, $end_day);
		$end->setTime(23, 59, 59);
		
		$qb = $this->em->createQueryBuilder();
		$qb
				->select(['gc', 'c', 't'])
				->from(\App\Geocaching::getClassName(), 'gc')
				->leftJoin('gc.country', 'c')
				->leftJoin('gc.type', 't')
				->where('gc.found BETWEEN :start AND :end')
				->orderBy('gc.found', 'DESC')
				->setParameters(["start" => $start, "end" => $end])
		;

		return $qb->getQuery()->getArrayResult();
	}

	public function getNewest($limit = 5) {

		$qb = $this->em->createQueryBuilder();
		$qb
				->select(['gc', 'c', 't'])
				->from(\App\Geocaching::getClassName(), 'gc')
				->leftJoin('gc.country', 'c')
				->leftJoin('gc.type', 't')
				//->innerJoin('gc.type', 'b')//, array('country' => 'c.country', 'type' => 'b.name'))
				//->
				->orderBy('gc.found', 'DESC')
				->setMaxResults($limit);


		return $qb->getQuery()->getArrayResult();
	}

	public function getCache($id, $inArray = FALSE) {
		if (!$inArray) {
			return $this->em->find(\App\Geocaching::getClassName(), $id);
		} else {
			$qb = $this->em->createQueryBuilder();
			$qb
					->select('gc')
					->from(\App\Geocaching::getClassName(), 'gc')
					->where(['id' => $id]);

			return $qb->getQuery()->getArrayResult();
		}
	}

	public function getType($type_id) {
		$typeEntity = $this->em->getDao(\App\GeocachingTypes::getClassName());

		return $typeEntity->findBy(array('idName' => $type_id))[0];
	}

	public function getTypeById($type_id) {
		$typeEntity = $this->em->getDao(\App\GeocachingTypes::getClassName());

		return $typeEntity->findBy(['id' => $type_id])[0];
	}

	/*	 * * COUNTRY ** */

	public function getCountry($country_id) {
		$typeEntity = $this->em->getDao(\App\GeocachingCountries::getClassName());

		return $typeEntity->findBy(array('originalName' => $country_id))[0];
	}

	public function getCountryById($country_id) {
		$typeEntity = $this->em->getDao(\App\GeocachingCountries::getClassName());

		return $typeEntity->findBy(['id' => $country_id])[0];
	}

	public function getAllCountries() {
		$typeEntity = $this->em->getDao(\App\GeocachingCountries::getClassName());

		return $typeEntity->findBy(array(), array('country' => 'asc'));
	}

	/*	 * * TYPE ** */

	public function getAllTypes() {
		$typeEntity = $this->em->getDao(\App\GeocachingTypes::getClassName());

		return $typeEntity->findBy(array(), array('position' => 'asc'));
	}

	/*	 * * REGIONS ** */

	public function getRegions($country = NULL) {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('DISTINCT i.region')
				->from('\App\Geocaching', 'i')
				->orderBy('i.region', 'asc');

		if (isset($country)) {
			$qb->where('i.country = :country')
					->setParameter('country', $country);
		}

		return $qb->getQuery()->getResult();
	}

	/*	 * * COUNTRIES **    */

	public function getCountries($region = NULL) {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('DISTINCT IDENTITY(i.country)')
				->from('\App\Geocaching', 'i')
				->orderBy('i.country', 'asc');

		if (isset($region)) {
			$qb->where('i.region = :region')
					->setParameter('region', $region);
		}

		return $qb->getQuery()->getResult();
	}

	public function getDistricts($country = NULL, $region = NULL, $town = NULL) {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('DISTINCT i.district')
				->from('\App\Geocaching', 'i')
				->orderBy('i.district', 'asc');

		if (isset($country)) {
			$qb->where('i.country = :country')
					->setParameter('country', $country);
		}
		if (isset($region)) {
			$qb->where('i.region = :region')
					->setParameter('region', $region);
		}

		if (isset($town)) {
			$qb->where('i.town = :town')
					->setParameter('town', $town);
		}

		return $qb->getQuery()->getResult();
	}

	public function getTowns($country = NULL, $region = NULL, $district = NULL) {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('DISTINCT i.town')
				->from('\App\Geocaching', 'i')
				->orderBy('i.town', 'asc');

		if (isset($country)) {
			$qb->where('i.country = :country')
					->setParameter('country', $country);
		}
		if (isset($region)) {
			$qb->where('i.region = :region')
					->setParameter('region', $region);
		}
		if (isset($district)) {
			$qb->where('i.district = :district')
					->setParameter('district', $district);
		}

		return $qb->getQuery()->getResult();
	}

	public function save($entity) {
		if (!$this->geocachingDao->find($entity->id)) {
			$this->geocachingDao->save($entity);
			$this->em->persist($entity);
			$this->em->flush();

			return TRUE;
		}
	}

	public function getLeastDate() {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('i.found')
				->from('\App\Geocaching', 'i')
				->orderBy('i.found', 'desc')
				->setMaxResults(1);

		return date("d.m.Y H:i", $qb->getQuery()->getResult()[0]['found']->getTimestamp());
	}

}
