<?php

namespace Dates;

/**
 * Description of Dates
 *
 * @author Administrator
 */
class Dates {
	
	const SK = "d.m.Y";
	
	public function validateDate($format, $date){
		$d = \DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
	public function validateRange($date1,$date2){
		$datetime1 = \DateTime::createFromFormat(self::SK, $date1);
		$datetime2 = \DateTime::createFromFormat(self::SK, $date2);
		
		if($datetime1 < $datetime2){
			return true;
		}
		
		return false;
	}
	
	public function toFormatedString($date,$format){
		if (!is_null($date)){
			if ($date instanceof \DateTime){
				return date($format, $date->getTimestamp());
			} 
			return date($format, $date);	
		} 
		return NULL;
	}
	
	
}
