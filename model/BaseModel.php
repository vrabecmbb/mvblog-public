<?php
namespace App\Model;

abstract class BaseModel extends \Nette\Object {
    
    protected $connection;
    protected $tableName;
    
    public function __construct(\Nette\Database\Context $connection) {
	$this->connection = $connection;
    }
    /**
     * 
     * @return type
     * @var Nette\Database\Table\Selection
     */
    protected function getTable(){
	//preg_match('#(\w+)Repository$#', get_class($this), $m);
        return $this->connection->table($this->tableName);
    }
    
    public function findAll(){
	return $this->getTable();
    }
    
    /**
     * @use Nette\Database\Table\Selection
     * @param array $by
     * @return 
     */
    public function findBy(array $by){
	return $this->getTable()->where($by);
    }
}