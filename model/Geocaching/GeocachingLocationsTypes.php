<?php

namespace Geocaching;

use \Nette\Object;

/**
 * Description of GeocachingLocationsTypes
 *
 * @author Administrator
 */
class GeocachingLocationsTypes {

    const COUNTRY_RANKING = 'country-ranking';
    const DISTRICT_RANKING = 'district-ranking';
    const REGION_RANKING = 'region-ranking';
    const TOWN_RANKING = 'town-ranking';

    public function getDefaultType() {
	return self::COUNTRY_RANKING;
    }

    public function getAllowedTypes() {
	return [
	    self::COUNTRY_RANKING,
	    self::DISTRICT_RANKING,
	    self::REGION_RANKING,
	    self::TOWN_RANKING
	];
    }

    public function getTypes() {
	$types = [
	    self::COUNTRY_RANKING => 'Krajiny',
	    self::DISTRICT_RANKING => 'Kraje',
	    self::REGION_RANKING => 'Okresy',
	    self::TOWN_RANKING => 'Obce'
	];

	return $types;
    }

}
