<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authenticator
 *
 * @author Administrator
 */
class PhotogalleryManager{
    
    private $em;
    private $photogalleryDao;

    public function __construct(Kdyby\Doctrine\EntityManager $em)
    {
        $this->em = $em;
        $this->photogalleryDao = $em->getDao(\App\PhotoCategory::getClassName());
    }
    
    public function getQueryBuilder()
    {
        return $this->photogalleryDao->createQueryBuilder();
    }
    
   public function getAll()
    {
	return $this->photogalleryDao->findAll();
    }
    
    /**
     * 
     * @param array $array entities and its values
     * @param array $order order of result
     * @return type
     */
    public function getBy($array,$order = array('category_id'=>'ASC'))
    {
        return $this->photogalleryDao->findBy($array,$order);
    }
    
    public function getIdForSafe($id)
    {
        $toRet = $this->photogalleryDao->findBy(array('category_id'=>$id));
        
        return $toRet[0];
    }
    
    public function getById($id)
    {
        $ret = $this->photogalleryDao->findBy(array('category_id'=>$id));
        
        return $ret[0];
    }
        
    public function save($entity)
    {
        $this->photogalleryDao->save($entity);
        $this->em->persist($entity);
        $this->em->flush();
    }
    
}