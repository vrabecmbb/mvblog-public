<?php

/**
 * Description of Authenticator
 *
 * @author Administrator
 */
class GeocachingStatsManager {

    private $em;
    private $geocachingDao;

    const NORTHEST = 'g.lat = (SELECT MAX(g.lat)';
    const SOUTHEST = 'g.lat = (SELECT MIN(g.lat)';
    const EASTEST = 'g.lon = (SELECT MAX(g.lon)';
    const WESTEST = 'g.lon = (SELECT MIN(g.lon)';
    const HIGHEST = 'g.altitude = (SELECT MAX(g.altitude)';
    const LOWEST = 'g.altitude = (SELECT MIN(g.altitude)';

    public function __construct(Kdyby\Doctrine\EntityManager $em) {
	$this->em = $em;
	$this->geocachingDao = $em->getDao(\App\Geocaching::getClassName());
    }

    public function getCount() {
	return $this->geocachingDao->countBy();
    }

    public function cacheTypeRanking() {
	$qb = $this->em->createQueryBuilder();
	$qb->addSelect('t.image, t.idName, t.name, t.color, count(g.id) AS county')
		->from('\App\GeocachingTypes', 't')
		->leftJoin('App\Geocaching', 'g', \Doctrine\ORM\Query\Expr\Join::LEFT_JOIN, 'g.type = t.id')
		->groupBy('g.type')
		->orderBy('county', 'DESC')
	;

	return $qb->getQuery()->getResult();
    }

    public function getNewestFromCountry($country = NULL, $limit = 5) {
	$qb = $this->em->createQueryBuilder();
	$qb->addSelect('g')
		->from('\App\Geocaching', 'g');
	if (isset($country)) {
	    $qb->where('g.country = :country')
		    ->setParameter('country', $country);
	}
	$qb->orderBy('g.added', 'desc')
		->setMaxResults($limit);

	return $qb->getQuery()->getResult();
    }

    private function getGeo($param, $limit = NULL, $country = NULL) {

	switch ($param) {
	    case 'northest': $param = self::NORTHEST;
		break;
	    case 'eastest': $param = self::EASTEST;
		break;
	    case 'southest': $param = self::SOUTHEST;
		break;
	    case 'westest': $param = self::WESTEST;
		break;
	    case 'highest': $param = self::HIGHEST;
		break;
	    case 'lowest': $param = self::LOWEST;
		break;
	}

	if (isset($country)) {
	    $countrySet = '=' . $country;
	} else {
	    $countrySet = 'IS NOT NULL';
	}

	$rsm = new Doctrine\ORM\Query\ResultSetMapping();

	$rsm->addScalarResult('id', 'id');
	$qb = $this->em->createNativeQuery('SELECT g.id FROM geocaching g WHERE ' . $param . ' FROM geocaching g WHERE g.country ' . $countrySet . ')LIMIT 1', $rsm);

	$id = $qb->getSingleResult()['id'];

	$geocaching = $this->geocachingDao->find($id);

	return $geocaching;
    }

    private function getAverageHeight($country = NULL) {
	$qb = $this->em->createQueryBuilder();
	$qb->select('AVG(a.altitude) AS average')
		->from('\App\Geocaching', 'a');

	if (isset($country)) {
	    $qb->where('a.country = :country')
		    ->setParameter('country', $country);
	}


	return $qb->getQuery()->getSingleScalarResult();
    }

    public function getGeoStats($country = NULL) {
	$toRet = [
	    'northest' => $this->getGeo('northest', $country),
	    'eastest' => $this->getGeo('eastest', $country),
	    'westest' => $this->getGeo('westest', $country),
	    'southest' => $this->getGeo('southest', $country),
	    'highest' => $this->getGeo('highest', $country),
	    'lowest' => $this->getGeo('lowest', $country),
	    'average' => $this->getAverageHeight($country)
	];

	return $toRet;
    }

    public function getCountryRanking($limit = NULL) {
	$qb = $this->em->createQueryBuilder();

	$qb->addSelect('gc, COUNT(gc.id) AS county')
		->from('\App\Geocaching', 'g')
		->join('\App\GeocachingCountries', 'gc', 'WITH', 'gc.id=g.country')
		->groupBy('gc.id')
		->orderBy('county', 'DESC')
		->setMaxResults($limit);

	return $qb->getQuery()->getScalarResult();
    }

    public function getRegionRanking($country = NULL, $limit = NULL) {
	$qb = $this->em->createQueryBuilder();

	$qb->addSelect('gc,g.region, COUNT(g.region) AS county')
		->from('\App\Geocaching', 'g')
		->join('\App\GeocachingCountries', 'gc', 'WITH', 'gc.id=g.country');

	if (isset($country)) {
	    $qb->where('gc.id = :country')
		    ->setParameter('country', $country);
	}

	$qb->groupBy('g.region')
		->orderBy('county', 'DESC');

	$qb->setMaxResults($limit);

	$toRet = $qb->getQuery()->getScalarResult();
	
	//spocitam kolko mam kesiek vo vysledku 
	$sum = 0;
	foreach($toRet as $gc)
	{
	  $sum += $gc['county']; 
	}
		
	//ak je kesiek viac ako vo vysledku vytvorim novy prvok Ostatne so zvyskom
	if ($sum < $this->getCount())
	{
	    $toRet['ostatne'] = ['county' => $this->getCount() - $sum, 'gc_image'=> 'Unknown.png', 'region' => 'Ostatné'];
	}
	
	return $toRet;
    }

    public function getDistrictRanking($country = NULL, $region = NULL, $limit = NULL) {
	$qb = $this->em->createQueryBuilder();

	$qb->addSelect('gc,g.district, COUNT(g.district) AS county')
		->from('\App\Geocaching', 'g')
		->join('\App\GeocachingCountries', 'gc', 'WITH', 'gc.id=g.country');

	if (isset($country)) {
	    $qb->andWhere('gc.id = :country')
		    ->setParameter('country', $country);
	}

	if (isset($region)) {
	    $qb->andWhere('g.region = :region')
		    ->setParameter('region', $region);
	}

	$qb->groupBy('g.district')
		->orderBy('county', 'DESC');

	$qb->setMaxResults($limit);

	$toRet = $qb->getQuery()->getScalarResult();
	
	//spocitam kolko mam kesiek vo vysledku 
	$sum = 0;
	foreach($toRet as $gc)
	{
	  $sum += $gc['county']; 
	}
		
	//ak je kesiek viac ako vo vysledku vytvorim novy prvok Ostatne so zvyskom
	if ($sum < $this->getCount())
	{
	    $toRet['ostatne'] = ['county' => $this->getCount() - $sum, 'gc_image'=> 'Unknown.png', 'district' => 'Ostatné'];
	}
	
	return $toRet;
    }

    public function getTownRanking($country = NULL, $region = NULL, $district = NULL, $limit = NULL) {
	$qb = $this->em->createQueryBuilder();

	$qb->addSelect('gc,g.town, g.district, g.region, COUNT(g.town) AS county')
		->from('\App\Geocaching', 'g')
		->join('\App\GeocachingCountries', 'gc', 'WITH', 'gc.id=g.country');

	if (isset($country)) {
	    $qb->andWhere('gc.id = :country')
		    ->setParameter('country', $country);
	}

	if (isset($region)) {
	    $qb->andWhere('g.region = :region')
		    ->setParameter('region', $region);
	}

	if (isset($district)) {
	    $qb->andWhere('g.district = :district')
		    ->setParameter('district', $district);
	}

	
	$qb->groupBy('g.town')
		->orderBy('county', 'DESC');

	$qb->setMaxResults($limit);
	
	$toRet = $qb->getQuery()->getScalarResult();
	
	//spocitam kolko mam kesiek vo vysledku 
	$sum = 0;
	foreach($toRet as $gc)
	{
	  $sum += $gc['county']; 
	}
		
	//ak je kesiek viac ako vo vysledku vytvorim novy prvok Ostatne so zvyskom
	if ($sum < $this->getCount())
	{
	    $toRet['ostatne'] = ['county' => $this->getCount() - $sum, 'gc_image'=> 'Unknown.png', 'town' => 'Ostatné'];
	}
	
	return $toRet;
    }

}
