<?php

/**
 * Description of DiaryModel
 *
 * @author Administrator
 */

namespace App\Model;

use Nette,
	Nette\Utils\Strings;

class DiaryModel {

	private $em;
    
    /**
     * @var \Kdyby\ElasticSearch\Client
     */
    private $client;

	public function __construct(\Kdyby\Doctrine\EntityManager $em, \Kdyby\ElasticSearch\Client $client) {
		$this->em = $em;
        $this->client = $client;
	}

	public function getAll() {
		return $this->em->getRepository(\App\Diary::getClassName())->findAll();
	}
	
	public function getByDate($date) {

		$dates = new \DateTime;
		$first = clone $dates;
		$first->setTime(0, 0, 0);

		$last = clone $date;
		$last->setTime(23, 59, 59);

		$qb = $this->em->createQueryBuilder();
		$qb->select('di')
				->from('\App\Diary', 'di')
				->where('di.fromDate >= :first')
				->andWhere('di.fromDate <= :last')
				->orderBy('di.fromDate', 'DESC')
				->setParameter('first', $first)
				->setParameter('last', $last)
		;

		return $qb->getQuery()->getResult();
	}

	function getByTime($month, $year) {

		$date = new \DateTime;
		$first = clone $date;
		$first->setTime(0, 0, 0);
		$first->setDate($year, $month, 1);

		$last = clone $date;
		$last->setTime(23, 59, 59);
		$last->setDate($year, $month + 1, 0);

		$qb = $this->em->createQueryBuilder();
		$qb->select('di')
				->from('\App\Diary', 'di')
				->where('di.fromDate >= :first')
				->andWhere('di.fromDate <= :last')
				->orderBy('di.fromDate', 'DESC')
				->setParameter('first', $first)
				->setParameter('last', $last)
		;

		return $qb->getQuery()->getResult();
	}

	/**
	 * Ziska mesiac a rok, nasledujuceho zapisu v denniku po aktualnom mesiaci a roku
	 * @param type $month
	 * @param type $year
	 * @return type
	 */
	public function getNextTime($month, $year) {
		//$last = mktime(23, 59, 00, $month + 1, 0, $year);

		$last = new \DateTime;
		$last->setDate($year, $month+1, 0);
		$last->setTime(23, 59, 59);

		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('di')
				->from('\App\Diary', 'di')
				->where('di.fromDate > :last')
				->setMaxResults(1)
				->orderBy('di.fromDate', 'ASC')
				->setParameter('last', $last)
		;

		$result = $qb->getQuery()->getResult();

		if (count($result) == 1)
			$time = \App\FrontModule\Presenters\DiariesPresenter::getTime($result[0]->fromDate->getTimestamp());
		else
			$time = FALSE;

		return $time;
	}

	/**
	 * Ziska mesiac a rok, predchadzajuceho zapisu v denniku po aktualnom mesiaci a roku
	 * @param type $month
	 * @param type $year
	 * @return type
	 */
	public function getPreviousTime($month, $year) {
		$first = new \DateTime;
		$first->setTime(23, 59, 59);
		$first->setDate($year, $month, 0);
		
		$qb = $this->em->createQueryBuilder();
		$qb->select('di')
				->from('\App\Diary', 'di')
				->where('di.fromDate <= :first')
				->orderBy('di.fromDate', 'DESC')
				->setParameter('first', $first)
				->setMaxResults(1);
		;

		$result = $qb->getQuery()->getResult();

		if (count($result) == 1) {
			$time = \App\FrontModule\Presenters\DiariesPresenter::getTime($result[0]->fromDate->getTimestamp());
		} else
			$time = FALSE;

		return $time;
	}

	/*
	 * Vrati konkretny dennik 
	 */

	public function getDiary($id) {
		return $this->em->find('\App\Diary', $id);
	}

	public function getLimited($limit) {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('d')
				->from(\App\Diary::getClassName(), 'd')
				->setMaxResults($limit)
				->orderBy('d.fromDate', 'DESC')
		;

		return $qb->getQuery()->getResult();
	}

	/**
	 * Aktualizacia zapisu dennicka
	 * @param type $data
	 */
	public function update($data) {
		$this->em->merge($data);
		$this->em->flush();
		return $this;
	}
    

	/**
	 * Vlozenie noveho prispecku do dennicka
	 * @param type $data
	 * @return type
	 */
	public function insert($diary) {
		$this->em->persist($diary);
		$this->em->flush();
		
		return $diary->getId();
	}

	//pridanie fotogalerie do clanku dennika
	public function addPhotogallery($id, $photogallery) {
		$diary = $this->em->find(\App\Diary::getClassName(), $id);
		$diary->photogallery = $photogallery;
		$this->em->persist($diary);
		$this->em->flush();
	}

	public function delete($id) {
		$diary = $this->em->find(\App\Diary::getClassName(),$id);
		$this->em->remove($diary);
		$this->em->flush();
	}

	/**
	 * Vrati najstarsi zapis, napr pre pouzitie v kalendari
	 */
	public function getOldest() {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('d')
				->from(\App\Diary::getClassName(), 'd')
				->setMaxResults(1)
				->orderBy('d.fromDate', 'ASC')
		;

		return $qb->getQuery()->getResult()[0];
	}

	/**
	 * Vrati najnovsi zapis v dennicku, pre pouzitie v kalendari
	 * @return type
	 */
	public function getNewest() {
		$qb = $this->em->createQueryBuilder();
		$qb->addSelect('d')
				->from(\App\Diary::getClassName(), 'd')
				->setMaxResults(1)
				->orderBy('d.fromDate', 'DESC')
		;

		return $qb->getQuery()->getResult()[0];
	}
    
    public function getTags($id){
        $qb = $this->em->createQueryBuilder();
		$qb->select('t')
        ->from(\App\Diary::getClassName(), 'd')
        ->innerJoin(\App\Tags::getClassName(), 't', 'WITH', 'd.id = :id')
        ->setParameter('id', $id);

		return $qb->getQuery()->getResult();
    }
    
    public function removeTag($diary_id, $tag_id){
        $diary = $this->em->getRepository(\App\Diary::getClassName())
                 ->find($diary_id);
                 
        $tag = $this->em->getRepository(\App\Tags::getClassName())
                 ->find($tag_id);    
        
        $diary->removeTag($tag);
        $this->em->persist($diary);
        $this->em->flush();         
                             
    }

}
