<?php

namespace Tags;

class Indexer{

    private $index;
    
    /** @var \Kdyby\ElasticSearch\Client */
	private $elastica;

    public function __construct($index, \Kdyby\ElasticSearch\Client $elastica){
        $this->index = $index;
        $this->elastica = $elastica;
    }
    
    public function clearDocuments($name, $type)
	{
		$query = new \Kdyby\ElasticSearch\Query;
		$this->elastica->getIndex($name)->getType($type)->deleteByQuery($query);
	}


	public function index($typeName, $id, $data)
	{
		$index	 = $this->elastica->getIndex($this->index);
		$type	 = $index->getType($typeName);

		$document = new \Elastica\Document($id, $data, $type, $this->index);
		$type->addDocument($document);
		$index->refresh();
	}


	public function indexWithoutRefresh($typeName, $id, $data)
	{
		$index	 = $this->elastica->getIndex($this->index);
		$type	 = $index->getType($typeName);

		$document = new \Elastica\Document($id, $data, $type, $this->index);
		$type->addDocument($document);
	}


	public function refreshIndex()
	{
		$index	 = $this->elastica->getIndex($this->index);
		$index->refresh();
	}

}