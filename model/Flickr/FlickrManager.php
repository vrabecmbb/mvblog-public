<?php

namespace Flickr;

use Nette,
	Nette\Utils\Strings,
	Nette\Caching\Cache,
	Nette\Caching\IStorage,
	\App\Model;

class FlickrManager {

	/** @var String */
	private $user_id;

	/** @var \IPub\Flickr\Client */
	private $flickr;
	private $exception;

	/** @var Cache * */
	private $cache;

	public function __construct($user_id, \IPub\Flickr\Client $flickr, \Nette\Caching\IStorage $storage) {
		$this->user_id = $user_id;
		$this->flickr = $flickr;
		$this->exception = FALSE;
		$this->cache = new Cache($storage);
	}

	private function getMyPhotos() {
		try {
			$gallery = $this->flickr->api('flickr.people.getPhotos', \IPub\Flickr\Api\Request::GET, ['user_id' => $this->user_id]);
		} catch (\Exception $e) {
			$this->exception = TRUE;
			return $e;
		}

		if ($this->exception === FALSE) {
			if ($gallery['stat'] == 'ok') {
				return $gallery['photos']['photo'];
			}
		}

		return $gallery['photos'];
	}

	private function mapPhoto($photo) {
		$toRet = [];

		$toRet['id'] = $photo['id'];
		$toRet['farm'] = $photo['farm'];
		$toRet['secret'] = $photo['secret'];
		$toRet['server'] = $photo['server'];
		$toRet['title'] = $photo['title'];
		$toRet['date'] = $photo['info']['photo']['dates']['taken'];
		$toRet['description'] = $photo['info']['photo']['description']['_content'];

		if (isset($photo['info']['photo']['location'])) {
			$toRet['location'] = $photo['info']['photo']['location'];
		}

		return $toRet;
	}

	public function getPhotos($limit = 5) {

		//$flickrPhotos = $this->cache->load("flickrPhotos"); 
		//if ($flickrPhotos == NULL){

		$photos = [];
		$myPhotos = $this->getMyPhotos();

		if ($this->exception) {
			return $myPhotos;
		} else {

			for ($photo = 0; $photo < $limit; $photo++) {
				$photos[$photo] = $myPhotos[$photo];
				$photos[$photo]['info'] = $this->flickr->api('flickr.photos.getInfo', \IPub\Flickr\Api\Request::GET, ['photo_id' => $myPhotos[$photo]['id']]);
			}

			$i = 0;
			foreach ($photos as $photo) {
				$finalPhoto[$i] = $this->mapPhoto($photo);
				$i++;
			}

			$this->cache->save("flickrPhotos", $finalPhoto);
			return $finalPhoto;
		}
		//} else {
		//from cache
		//return $flickrPhotos;
		//}
	}

	public function getCachedPhotos() {

		$flickrPhotos = $this->cache->load("flickrPhotos");

		if ($flickrPhotos != NULL) {
			return $flickrPhotos;
		}

		return $this->getPhotos(12);
	}

}
