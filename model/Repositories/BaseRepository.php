<?php

class BaseRepository extends \Doctrine\ORM\EntityRepository {
	
	protected $user = null;
	
	public function getDataSource(User $user) {
		$query = $this->_em->createQueryBuilder();
		$query->select('e')->from($this->_entityName, 'e');
		$query->where('e.user = :user')->setParameter('user', $user);
		return $query;
	}
	
	public function fetchPairs($key, $value = null) {
		$collection = array();
		foreach ($this->findAll() as $entity) {
			$collection[$entity->$key] = !empty($value) ? $entity->$value : $entity;
		}
		return $collection;
	}
}