<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Authenticator
 *
 * @author Administrator
 */
class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator {
    
    /** @var \Doctrine\ORM\EntityRepository */
    private $admins;

    private $em;
    private $articlesDao;

    public function __construct(Kdyby\Doctrine\EntityManager $em)
    {
        $this->em = $em;
        $this->admins = $em->getDao(App\Admin::getClassName());
        // $this->articlesDao = $em->getDao(App\Article::getClassName()); // for older PHP
    }
    
    public function authenticate(array $credentials)
    {
        
        list($username, $password) = $credentials;
	$admin = $this->admins->findOneBy(array('login' => $username));
              
	$login    = $credentials[self::USERNAME];
	$password = $credentials[self::PASSWORD];
        
	if (!$admin) {
            throw new \Nette\Security\AuthenticationException("User $username not found.", self::IDENTITY_NOT_FOUND);
	}

	if ($admin->password !== $this->calculateHash($password)) {
		throw new \Nette\Security\AuthenticationException("Invalid password.", self::INVALID_CREDENTIAL);
	}
		
	$identity = new \Nette\Security\Identity($admin->username, $admin->role);
	$identity->name = $admin->username;

	return $identity;
    }

    private function calculateHash($string) {
        return hash('sha256', $string);
    }
    
}
