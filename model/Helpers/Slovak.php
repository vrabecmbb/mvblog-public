<?php

namespace Helpers;

class Slovak{
	
	public function declension($number,$one,$twoToFour,$five){
		switch ($number) {
			case 1: $toRet = $one;
				break;
			case 2:
			case 3:
			case 4: $toRet = $twoToFour;
				break;
			default: $toRet = $five;	
		}
		
		return $number . ' ' . $toRet;
	}
	
}

