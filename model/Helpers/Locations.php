<?php

namespace Helpers;

class Locations{
    
    /** @var \GeocachingManager **/
    private $geocachingManager;
    
    public function __construct(\GeocachingManager $geocachingManager){
        $this->geocachingManager = $geocachingManager;
    }
    
    public function getRegions($country = NULL)
    {
        $regions = $this->geocachingManager->getRegions($country);
        
        $toRet = [];
        foreach ($regions as $region) {
            $toRet[] = $region['region'];
        };
        
        return $toRet;
    }
    
    public function getCountries($region = NULL)
    {
        $countries = $this->geocachingManager->getCountries($region);
        
        $toRet = [];
        foreach ($countries as $country) {
            $toRet[] = $country[1];
        };
        
        return $toRet;
    }
    
    public function getDistricts($region = NULL)
    {
        $districts = $this->geocachingManager->getDistricts(NULL, $region);
        
        $toRet = [];
        foreach ($districts as $district) {
            $toRet[] = $district['district'];
        };
        
        return $toRet;
    }
    
    public function getTowns($district = NULL)
    {
        $towns = $this->geocachingManager->getTowns(NULL, NULL, $district);
        
        $toRet = [];
        foreach ($towns as $town) {
            $toRet[] = $town['town'];
        };
        
        return $toRet;
    }
    
    public function setQueryFilter($filter)
    {
        $types = ['country','region','district','town'];
        
        $toRet = [];
        foreach ($types as $type)
        {
            if (array_key_exists($type, $filter))
            {
                $toRet[$type] = $filter[$type];
            } else {
                $toRet[$type] = '';
            }
        }
        
        return $toRet;
    }
    
    
}
