<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//require_once '../libs/statistics.php';
namespace App\Geocaching;
//require dirname('libs/statistics.php');

class Geocaching extends \Nette\Application\UI\Control
{
    protected $geocachingModel;
    
    public function __construct(\App\Model\GeocachingModel $geocachingModel) {
        parent::__construct();
        $this->geocachingModel = $geocachingModel;
    }
    
    public function render()
    {   
        $this->template->setFile(__DIR__.'\geocaching.latte');   
        $this->template->geocaching = $this->geocachingModel;
        
        $this->template->cachesNum = 24;
        $this->template->newestCaches = $this->geocachingModel->getNewest(5);
 
        /** SV. STRANY **/
        $this->template->eastest = $this->geocachingModel->getEastest();
        $this->template->northest = $this->geocachingModel->getNorthest();
        $this->template->westest = $this->geocachingModel->getWestest();
        $this->template->southest = $this->geocachingModel->getSouthest();
        
        /** NAJNIZSIE, NAJVYSSIE **/
        $this->template->highest = $this->geocachingModel->getHighest();
        $this->template->lowest = $this->geocachingModel->getLowest();
        
        /** RENKINGY **/
        $this->template->countryRanking = $this->geocachingModel->countryRanking();
        $this->template->regionRankong = $this->geocachingModel->regionRanking();
        $this->template->districtRankong = $this->geocachingModel->districtRanking();
        $this->template->townRanking = $this->geocachingModel->townRanking();
        
        //$this->template->countryRanking = $this->geocachingModel->getCountryRanking(5);
        //$this->template->getHighest = $this->geocachingModel->getHighest();
        
        $this->template->render();
    }
    
}
