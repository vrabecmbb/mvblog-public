<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * All entity classes must be declared as such.
 *
 * @ORM\Entity
 * @ORM\Table(name="tags")
 */
class Tags extends \Kdyby\Doctrine\Entities\BaseEntity{

    public function __construct() {
        $this->diaries = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;
    
    /**
	 * @ORM\ManyToMany(targetEntity="Diary", inversedBy="diaries",cascade={"persist"})
	 * @ORM\JoinTable(
	 *     name="diary_tag",
	 *     joinColumns={
	 *         @ORM\JoinColumn(name="diary_id", referencedColumnName="id")
	 *     },
	 *     inverseJoinColumns={
	 *         @ORM\JoinColumn(name="tags_id", referencedColumnName="id")
	 *     }
	 * )
	 */
    public $diaries;
    
    /**
     * @ORM\Column (type="string",unique=true) 
     */
    public $name;
    
    public function addDiaries(Diary $diary)
    {
        $this->diary->add($diary);
        return $this;
    }
    
    public function getDiaries(): \Doctrine\Common\Collections{
        return $this->diary;
    }
    
    public function removeDiary($diary){
        $this->diary->removeElement($diary);
    }
    

}
    