<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Geocaching_countries
 *
 * @author Administrator
 * @ORM\Entity
 * @ORM\Table(name="geocaching_types")
 */
class GeocachingTypes extends \Kdyby\Doctrine\Entities\BaseEntity
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;
    
    /**
     * @ORM\Column(type="string")
     */
    public $idName;
    
    /**
     * @ORM\Column (type="string") 
     */
    public $name;
    
    /**
     * @ORM\Column (type="integer") 
     */
    public $position;
    
    /**
     * @ORM\Column (type="string") 
     */
    public $image;
    
     /**
     * @ORM\Column (type="string") 
     */
    public $color;
    
    
    
}
