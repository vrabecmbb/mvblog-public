<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace components;

use Nette\Application\UI\Control,
    Grido\Components\Filters\Filter;

/**
 * Description of PhotogalleryGrid
 *
 * @author Administrator
 */
class PhotogalleryGrid extends Control{
    
    /** @var \PhotogalleryManager */
    private $photogalleryManager;
    
    private $photogalleryRepository;
    private $categories;
    
    public function __construct(\PhotogalleryManager $photogalleryManager) {
        parent::__construct();
        $this->photogalleryManager = $photogalleryManager;
        $this->categories = $this->photogalleryManager->getAll();
    }
    
    public function createComponentGrid($name) {
        $grid = new \Grido\Grid($this, $name);

        $model = new \Grido\DataSources\Doctrine(
            $this->photogalleryManager->getQueryBuilder() // We need to create query builder with inner join.
                ->select('p')
                ->from('\App\PhotoCategory', 'p')
                ->leftJoin('p.parent','c'),array('children' => 'c.name')
                );
        $grid->model = $model;
        

        $grid->addColumnText('id', 'Id');
            //->setFilterText()
            //   ->setSuggestion();

        $grid->addColumnText('category_name', 'Názov')
            ->setSortable();
            //->setFilterText()
            //  ->setSuggestion();

        $grid->addColumnText('parent', 'Rodič')
           ->setSortable();
            //->cellPrototype->class[] = 'center';


        $grid->addActionHref('edit', 'Edit')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Delete')
            ->setIcon('trash')->setCustomRender(function($item,$el) {
                    $el->href('#test');
                    $el->addAttributes(array('rel' =>'deleteForm','category-id' => $item->category_id));
                    return $el;
                });

    }
    
    public function render()
    {
        $template = $this->template;
       
        $template->setFile(__DIR__ . '/photogallerygrid.latte');
        $template->categories = $this->categories;
     
        $template->render();
    }
    
}
