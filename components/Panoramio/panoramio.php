<?php

namespace Components;

class Panoramio extends \Components\BaseControl {

	private $url;
	private $panoramio;
	
	public function __construct($panoramio)
	{
		$this->panoramio = $panoramio;
		$this->url = "http://www.panoramio.com/map/get_panoramas.php?set=".$panoramio["user_id"]."&from=0&to=20&minx=-180&miny=-90&maxx=180&maxy=90&size=medium&mapfilter=true&callback=panoramioCallback";
	}
	
	public function getUrl(){
		return $this->url;
	}


	public function render() {
		$this->template->url = $this->url;
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->render();
	}

}
