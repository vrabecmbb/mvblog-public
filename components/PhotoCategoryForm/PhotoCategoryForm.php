<?php

namespace Components;

use Nette\
Nette\Forms\Form,
    \App\PhotoCategory,
    Nette\Utils\Image;

/**
 * Description of PhotoCategoryForm
 *
 * @author Administrator
 */
class PhotoCategoryForm extends \Components\BaseControl {

    /** @var PhotogalleryManager @inject */
    public $photogalleryManager;

    /** @var integer */
    private $id;
    private $imageParams;
    private $baseDir;

    /** @var \App\PhotoCategory */
    private $photocategory;

    /** @var array */
    public $onUpdate = [];

    /** @var array */
    public $onCreate = [];

    /** @var array */
    public $onError = [];

    function __construct(\PhotogalleryManager $photogalleryManager) {
        parent::__construct();
        $this->photogalleryManager = $photogalleryManager;
    }

    function setImageParams($imageParams) {
        $this->imageParams = $imageParams;
    }

    function setBaseDir($baseDir) {
        $this->baseDir = $baseDir;
    }

    function setPhotocategory($id) {
        $this->photocategory = $this->photogalleryManager->getById($id);
    }
    
    function setId($id) {
        $this->id = $id;
    }

    
    public function createComponentForm() {

        //$form = $this->formFactory->create();
        $form = new \Nette\Application\UI\Form();

        $form->addHidden('id');

        $form->addText('cat_name', 'Kategória')
                ->setRequired('Musí byť vybraný názov kategórie')
        ;

        $categoryOptions = $this->getCategoriesDropodown();
        $form->addSelect('cat_parent', 'Rodič', $categoryOptions)
                ->setRequired('Prosím vyber rodičovskú kategóriu')
        ;

        $form->addUpload('cat_image', 'Obrázok: ')
                ->setRequired('Prosím vyber obrázok kategórie')
        ;

        $form->addSubmit('cat_send');

        $form->onSuccess[] = [$this, 'processForm'];

        $form->onError[] = [$this, 'onError'];

        if (!is_null($this->photocategory)) {
            $defaults = $this->prepareEditCategoryDefaults();
            $form['cat_image']->setRequired(FALSE);
        } else {
            $defaults = $this->prepareNewCategoryDefaults();
        }

        $form->setDefaults($defaults);

        $form->onError[] = function(\Nette\Application\UI\Form $form) {
            $this->fireCallbacks($this->onError);
        };
        
        return $form;
    }

    public function setDefaults($defaults) {
        $this['form']->setDefaults($defaults);
    }

    public function processForm(\Nette\Application\UI\Form $form) {

        $values = $form->getValues();

        $imageSize = $this->imageParams;

        if (is_numeric($values->id)) {
            $photocategory = $this->photocategory = $this->photogalleryManager->getById($values->id);
        }

        if (!$values['cat_image']->isOk() && is_null($this->photocategory) ) {
            $this->fireCallbacks($this->onError, ['Obrázok kategórie nebol vybraný.']);
        }

        if (!isset($this->photocategory)) {
            $category = new \App\PhotoCategory();
        } else {
            $category = $this->photogalleryManager->getById($this->photocategory->id);
        }

        $category->setName($values['cat_name']);
        if (isset($values['cat_parent'])) {
            $category->setParentId($this->photogalleryManager->getIdForSafe($values['cat_parent']));
        } else {
            $category->setParentId(NULL);
        }

        try {
            //Spracovanie obrazku
            if ($values['cat_image']->isOk()) {
                $image = Image::fromFile($values['cat_image']->getTemporaryFile());

                $image->resize($imageSize['width'], $imageSize['height'], Image::FILL);

                $newImage = $imageSize['folder'] . '/' . $values['cat_name'] . '.jpg';
                $image->save($imageSize['folder'] . '/' . $values['cat_name'] . '.jpg', 80, Image::JPEG);

                $imageSize = getimagesize($newImage);

                $imageData = [
                    'url' => '/' . $newImage,
                    'width' => $imageSize[0],
                    'height' => $imageSize[1]
                ];

                $category->setPhotogallery($imageData);
            }
            
            $this->photogalleryManager->save($category);
        } catch (\Exception $e) {
            dump($e);exit;
            $this->fireCallbacks($this->onError, [$e]);
        }
        
        if (isset($this->photocategory)) {
            $this->fireCallbacks($this->onUpdate);
        } else {
            $this->fireCallbacks($this->onCreate);
        }
    }

    private function prepareNewCategoryDefaults() {
        return $defaults['cat_parent'] = $this->getCategoriesDropodown();
    }

    private function prepareEditCategoryDefaults() {
        $category = $this->photocategory;

        $id = $category->category_id;
        
        $defaults = [
            'id' => $category->category_id,
            'cat_name' => $category->category_name,
            'cat_parent' => ($category->parent ? $category->parent->category_id : NULL),
            'cat_image' => $category->photogallery,
        ];
       
        return $defaults;
    }

    private function getCategoriesDropodown() {
        $categories = $this->photogalleryManager->getAll();

        $toRet[0] = 'Žiadny rodič';

        foreach ($categories as $category) {
            $toRet[$category->id] = $category->name;
        }

        return $toRet;
    }

    public function create() {
        return $this;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/CRUDCategory.latte');
        
        $this->template->kategoria = $this->photocategory;
        
        $this->template->render();
    }

}
