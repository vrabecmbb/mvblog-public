<?php

namespace Components;

/**
 * Description of GeocachingGlobalStats
 *
 * @author Administrator
 */
class GeocachingGlobalStats extends \Components\BaseControl{
    
    /**  @var \GeocachingManager @inject  **/
    private $geocachingManager;
    
    /** @ var \GeocachingStatsManager **/
    private $geocachingStatsManager;
    
    /** @var array **/
    private $chartColors;
    
    public function __construct($chartColors, \GeocachingManager $geocachingManager, \GeocachingStatsManager $geocachingStatsManager) {
		$this->chartColors = $chartColors;
		$this->geocachingManager = $geocachingManager;
		$this->geocachingStatsManager = $geocachingStatsManager;
		$this->prepareTypeChart();
    }

	private function prepareTypeChart(){
		
		$count = $this->geocachingStatsManager->getCount();
		$types = $this->geocachingStatsManager->cacheTypeRanking();
		
		$coeficient = 92/100;
		
		$l = count($types);
		for($i = 0; $i < $l; $i++){
			$types[$i]['width'] = round((($types[$i]['county'] / $count) * $coeficient)*100,2); 
		}
		
		return $types;
	} 
	
	private function prepareCountryChart(){
		
		$count = $this->geocachingStatsManager->getCount();
		$countries = $this->geocachingStatsManager->getCountryRanking(NULL, 7);
		
		$coeficient = 92/100;
		
		$l = count($countries);
		for($i = 0; $i < $l; $i++){
			$countries[$i]['width'] = round((($countries[$i]['county'] / $count) * $coeficient)*100,2); 
		}
		
		return $countries;
	} 
	
	private function prepareRegionChart(){
		
		$region_num = 12;
		
		$count = $this->geocachingStatsManager->getCount();
		$regions = $this->geocachingStatsManager->getRegionRanking(NULL, $region_num);
		
		$coeficient = 92/100;
		
		$l = count($regions);
		$max = $l > $region_num ? $l-1 : $l;

		for($i = 0; $i < $max; $i++){
			$regions[$i]['width'] = round((($regions[$i]['county'] / $count) * $coeficient)*100,2); 
		}
		
		if ($l > $region_num){
			$regions['ostatne']['width'] = round((($regions['ostatne']['county'] / $count) * $coeficient)*100,2);
		}
		
		return $regions;
	}
	
	private function prepareDistrictRanking(){
		
		$count = $this->geocachingStatsManager->getCount();
		$districts = $this->geocachingStatsManager->getDistrictRanking(NULL, NULL, 15);
		
		$coeficient = 92/100;
		
		$l = count($districts);
		
		for($i = 0; $i < $l-1; $i++){
			if ($districts[$i]){
				$districts[$i]['width'] = round((($districts[$i]['county'] / $count) * $coeficient)*100,2); 
			} else {
				$districts['ostatne']['width'] = round((($districts[$i]['county'] / $count) * $coeficient)*100,2); 
			}
		}
		
		return $districts;
		
		//$this->geocachingStatsManager->getDistrictRanking(NULL, NULL, 7)
	}
	
	private function prepareTownRanking(){
		
		$count = $this->geocachingStatsManager->getCount();
		
		$towns = $this->geocachingStatsManager->getTownRanking(NULL, NULL, NULL, 15);
		
		$coeficient = 92/100;
		
		$l = count($towns);
		
		for($i = 0; $i < $l-1; $i++){
			if ($towns[$i]){
				$towns[$i]['width'] = round((($towns[$i]['county'] / $count) * $coeficient)*100,2); 
			} else {
				$towns['ostatne']['width'] = round((($towns[$i]['county'] / $count) * $coeficient)*100,2); 
			}
		}
		
		//dump($towns);
		//exit;
		return $towns;
		
	}
	
	
    public function render() {
	$this->template->setFile(__DIR__.'/template2.latte');
	
	$this->template->chartColors = $this->chartColors;
	
	$this->template->gcCount = $this->geocachingStatsManager->getCount();
        $this->template->gcTypeChart = $this->prepareTypeChart();
	$this->template->countryRanking = $this->prepareCountryChart();	
	//$this->template->countryRanking = $this->geocachingStatsManager->getCountryRanking(NULL, 7);
	$this->template->regionRanking = $this->prepareRegionChart();//$this->geocachingStatsManager->getRegionRanking(NULL, 0, 7);
	//$this->template->regionRanking1 = $this->prepareRegionChart(8,15);//$this->template->regionRanking2 = $this->geocachingStatsManager->getRegionRanking(NULL, 8, 7);
	$this->template->districtRanking = $this->prepareDistrictRanking();
	//$this->template->townRanking = $this->geocachingStatsManager->getTownRanking(NULL, NULL, NULL, 7);
	$this->template->townRanking = $this->prepareTownRanking();
	$this->template->gcStats = $this->geocachingStatsManager->getGeoStats();
	
	$this->template->render();
    }

}
