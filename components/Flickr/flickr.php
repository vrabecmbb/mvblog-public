<?php

namespace Components;

use Nette\Caching\Cache;

class Flickr extends \Components\BaseControl {

	/** @var \Flickr\FlickrManager @inject */
	public $flickrManager;
	
	public function getPhotos()
	{
		return $this->flickrManager->getCachedPhotos(12);
	}
	
	public function render() {		
		$this->template->photos = $this->getPhotos();
		$template = $this->getPhotos() instanceof \Exception ? '/template_err.latte' : '/template.latte' ;
		$this->template->setFile(__DIR__ . $template);
		$this->template->render();
	}

}
