<?php

namespace Components;

class DiarySearchForm extends BaseControl {
	
	/** @var array */
	public $onResults = [];
	
	protected function createComponentForm()
	{
		$form = $this->formFactory->create();

		$form->addText('search', 'Search');
		$form->addHidden('searchType','search_type');
		$form->addSubmit('btnSubmit');

		$form->onSuccess[] = [$this,'onSuccess'];

		return $form;
	}

	public function setSearch($search,$type)
	{
		$this['form']['search']->setValue($search);
		$this['form']['searchType']->setValue($type);
		return $this;
	}

	public function handleSearch($search)
	{
		$this->fireCallbacks($this->onResults, [$search]);
	}

	public function onSuccess(\Nette\Application\UI\Form $form)
	{
		$values = $form->getValues();

		$search = $values->search;

		$this->fireCallbacks($this->onResults, [$search]);
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/template.latte');

		$this->template->render();
	}

}
