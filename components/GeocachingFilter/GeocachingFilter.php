<?php

namespace Components;

/**
 * Description of GeocachingGlobalStats
 *
 * @author Administrator
 */
class GeocachingFilter extends \Components\BaseControl{
     
    /**  @var \GeocachingManager @inject  **/
    private $geocachingManager;
    
	private $gcTypes;
	
	private $gcCountries;
	
	private $gcRegions;
	
	private $gcDistricts;
	
	private $gcTowns;
	
	private $type;
	
	private $country;
	
	private $region;
	
	private $district;
	
	private $town;
	
	/** @var \Nette\Http\SessionSection */
	private $sessionSection;

	/** @var \Nette\Http\Session */
	private $session;

	
    function __construct(\GeocachingManager $geocachingManager, \Nette\Http\Session $session) {
		$this->session = $session;
		$this->geocachingManager = $geocachingManager;
		$this->prepareGcTypes();
		$this->prepareGcCountries();
		
		$this->sessionSection = $this->session->getSection('geocaching');
		
		$this->redrawFilter();
	}
	
	function setType($type) {
		$this->type = $type;
		
		if ($type > 0){
			$ret = $this->geocachingManager->getTypeById($type);
			$this->sessionSection->type = $ret;
		} else {
			$this->sessionSection->type = NULL;
			$this->type = NULL;	
		}
	}

	function setCountry($country) {
		$this->country = $country;
		
		if ($country > 0){
			$ret = $this->geocachingManager->getCountryById($country);
			$this->sessionSection->country = $ret;
		} else {
			$this->sessionSection->country = NULL;
			$this->country = NULL;	
		}
	}

	function setRegion($region) {
		$this->region = $region;
		
		if ($region != 'Všetky'){
			$this->sessionSection->region = $region;
		}
		
	}

	function setDistrict($district) {
		$this->district = $district;
		
		if ($district != 'Všetky'){
			$this->sessionSection->district = $district;
			$this->district = NULL;
		}
		
	}

	function setTown($town) {
		$this->town = $town;
		
		if ($town != 'Všetky'){
			$this->sessionSection->town = $town;
			$this->town = NULL;
		}
	}
	
	function getType() {
		return $this->sessionSection->type;
	}

	function getCountry() {
		return $this->sessionSection->country;
	}

	function getRegion() {
		return $this->sessionSection->region;
	}

	function getDistrict() {
		return $this->sessionSection->district;
	}

	function getTown() {
		return $this->sessionSection->town;
	}

	public function setFilter($parameter){
		
		if (isset($parameter['gcType'])){
			$this->setType((int)$parameter['gcType']);
		}
		
		if (isset($parameter['gcCountry'])){
			$this->setCountry((int)$parameter['gcCountry']);
			$this->prepareGcRegions($this->country);
			
			$this->sessionSection->offsetUnset('region');
			$this->sessionSection->offsetUnset('district');
			$this->sessionSection->offsetUnset('town');
		}
		
		if (isset($parameter['gcRegions'])){
			$this->setRegion($parameter['gcRegions']);
			$this->prepareGcDistricts($this->region);
			
			$this->sessionSection->offsetUnset('district');
			$this->sessionSection->offsetUnset('town');
		}
		
		if (isset($parameter['gcDistricts'])){
			$this->setDistrict($parameter['gcDistricts']);
			$this->prepareGcTowns($this->district);
		}
		
		if (isset($parameter['gcTowns'])){
			$this->setTown($parameter['gcTowns']);
			$this->prepareGcTowns($this->district);
		}
		
		$this->redrawFilter();
		
	}

	private function redrawFilter(){
		//$this->gcTypes = NULL;
		//$this->gcCountries = NULL;
		$this->gcRegions = NULL;
		$this->gcDistricts = NULL;
		$this->gcTowns = NULL;
		
		if ($this->sessionSection->country){
			$this->prepareGcRegions($this->sessionSection->country);
		}
		
		if ($this->sessionSection->region){
			$this->prepareGcDistricts($this->sessionSection->region);
		}
		
		if ($this->sessionSection->district){
			$this->prepareGcTowns($this->sessionSection->district);
		}
	}	
	
	private function prepareGcTypes(){
		$types = $this->geocachingManager->getAllTypes();
		
		$all = new \stdClass();
		$all->image = '0.gif';
		$all->id = 0;
		$all->name = 'Všetky';
		
		array_unshift($types, $all);
		
		$this->gcTypes = $types;
	}
	
	private function prepareGcCountries(){
		$countries = $this->geocachingManager->getAllCountries();
		
		$all = new \stdClass();
		$all->image = 'United-Nations.png';
		$all->id = 0;
		$all->country = 'Všetky';
		
		array_unshift($countries, $all);
		
		$this->gcCountries = $countries;
	}
	
	private function prepareGcRegions($country){
		
		$regions = $this->geocachingManager->getRegions($country);
		
		$regions['all'] = ['region' => 'Všetky'];
		
		$this->gcRegions = $regions;	
	}
	
	private function prepareGcDistricts($region){
		
		$districts = $this->geocachingManager->getDistricts(NULL, $region);
		
		$districts['all'] = ['district' => 'Všetky'];
		
		$this->gcDistricts = $districts;	
	}
	
	private function prepareGcTowns($district){
		
		$towns = $this->geocachingManager->getTowns(NULL, NULL, $district);
		
		$towns['all'] = ['town' => 'Všetky'];
		
		$this->gcTowns = $towns;	
	}

	public function handleRemoveSession(){
		
		$this->sessionSection->remove();
				
		$this->redirect('this');
	}
	
	public function render(){
		$this->template->setFile(__DIR__.'/template.latte');
		
		$this->template->gcTypes = $this->gcTypes;
		$this->template->gcTypeSelected = $this->sessionSection->type;
				
		$this->template->gcCountries = $this->gcCountries;
		$this->template->gcCountrySelected = $this->sessionSection->country;
		
		$this->template->gcRegions = $this->gcRegions;
		$this->template->gcRegionSelected = $this->sessionSection->region;
		
		$this->template->gcDistricts = $this->gcDistricts;
		$this->template->gcDistrictSelected = $this->sessionSection->district;
		
		$this->template->gcTowns = $this->gcTowns;
		$this->template->gcTownSelected = $this->sessionSection->town;
		
		$this->template->session = $this->sessionSection;
		
		$this->template->render();
	}
}
