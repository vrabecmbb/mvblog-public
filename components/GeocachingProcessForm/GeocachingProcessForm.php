<?php

namespace Components;

class GeocachingProcessForm extends BaseControl {

	/** @var String */
	private $locFile;
	private $addedCaches;

	/** @var \GeocachingManager */
	private $geocachingManager;

	/** @var array */
	public $onSuccess = [];

	/** @var array */
	public $onError = [];

	function __construct(\GeocachingManager $geocachingManager) {
		$this->geocachingManager = $geocachingManager;
	}

	public function createComponentForm() {

		$form = new \Nette\Application\UI\Form();
		$form
				->addUpload('locFile', 'Súbor')
				//->addRule(\Nette\Forms\Form::MIME_TYPE, 'Súbor musí byť GPX', 'application/gpx')
		;

		$form
				->addCheckbox('oldVersion', 'Použiť starú verziu')
				->setDefaultValue(TRUE)
		;
		
		$form
				->addCheckbox('mapjson', 'Vytvoriť geoJSON')
				->setDefaultValue(TRUE)
		;

		$form->addSubmit('submit', 'Odoslať');

		$form->onSuccess[] = [$this, 'processForm'];

		if ($form->hasErrors()) {
			$this->fireCallbacks($this->onError,'Error');
		}

		return $form;
	}

	public function processForm($form) {
		$values = $form->getValues();

		try{
			if ($values['oldVersion'] == TRUE) {
				$this->processGeocachingAdd($form);
			} else {
				$reader = new \Libs\GeocacheReader($values->locFile);
				$waypoints = $reader->processFile();

				$saved = 0;
				foreach ($waypoints as $wpts) {

					$gc = new \App\Geocaching();
					$gc->id = (string) $wpts['wpt']->name;

					// Now check whether cache is not in database
					if (!$this->geocachingManager->getCache($gc->id) && !(empty((string) $wpts['gpxg']->GeogetExtension->Found))) {

						$gc->type = $this->geocachingManager->getType((string) $wpts['groundspeak']->cache->type);
						$gc->name = (string) $wpts['groundspeak']->cache->name;
						$gc->owner = (string) $wpts['groundspeak']->cache->owner;
						$gc->lon = (float) $wpts['wpt']->attributes()->lon;
						$gc->lat = (float) $wpts['wpt']->attributes()->lat;
						$gc->region = (string) $wpts['groundspeak']->cache->state;
						$gc->country = $this->geocachingManager->getCountry((string) $wpts['groundspeak']->cache->country);
						$gc->difficulty = (string) $wpts['groundspeak']->cache->difficulty;
						$gc->terrain = (string) $wpts['groundspeak']->cache->terrain;
						$gc->found = new \DateTime((string) $wpts['gpxg']->GeogetExtension->Found);
						$gc->altitude = round($this->getGeo($gc->lat, $gc->lon)['altitude']);
						$gc->district = $this->getDistrict($gc->lat, $gc->lon);
						$gc->town = $this->getTown($gc->lat, $gc->lon);

						if ($this->geocachingManager->save($gc)) {
							$saved++;
						}
					}
				}
				$this->fireCallbacks($this->onSuccess, [$saved,$form->getValues()]);
			}
		} catch (ErrorException $e){
			$this->fireCallbacks($this->onError,$e);
		}
	}

	public function onError($form) {
		$this->fireCallbacks($this->onError, [$form->errors]);
	}

	
	public function processGeocachingAdd(\Nette\Application\UI\Form $form) {
		$values = $form->getValues();
		$file = $values->locFile->name;

		try {
			if (pathinfo($file)['extension'] != 'loc')
				throw new \Exception('Súbor nie je LOC');

			$this->locFile = $values->locFile->getContents();

			$this->removeCdata();

			$xml = simplexml_load_string($this->locFile)->children();

			foreach ($xml as $geocache) {
				$array[] = $this->processGeocache($geocache);
			}

			$array = array_reverse($array);

			foreach ($array as $geocache) {
				$gc = new \App\Geocaching;
				$gc->id = $geocache['id'];
				$gc->name = $geocache['name'];
				$gc->lat = $geocache['lat'];
				$gc->lon = $geocache['long'];
				$gc->country = $this->geocachingManager->getCountry($geocache['geocaching_countries_id']);
				$gc->region = $geocache['region'];
				$gc->district = $geocache['district'];
				$gc->town = $geocache['town'];
				$gc->altitude = $geocache['altitude'];
				$gc->owner = $geocache['owner'];
				$gc->type = $this->geocachingManager->getType($geocache['geocaching_types_id']);

				$gc->added = time();
				//$geocache['link'] = 'http://www.geocaching.com/seek/cache_details.aspx?wp='.$geocache['id'];
				if ($this->geocachingManager->save($gc)) {
					$this->addedCaches++;
					sleep(2);
				}
			}

			$this->flashMessage('Do databázy bolo pridaných ' . $this->addedCaches . ' kešiek ;)', 'ok');
		} catch (\Exception $ex) {
			$form->addError($ex->getMessage());
		}
	}

	private function getHTMLByClass($id, $html) {
		$dom = new \DOMDocument;
		libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		$node = $dom->getElementById($id);
		if ($node) {
			$cacheDetail = $dom->saveXML($node);
			$img_start = strpos('<img title=', $cacheDetail);
			return $cacheDetail; //substr($cacheDetail, $img_start);
//$img_stop = strpos('>', $cacheDetail)
		}
	}

	private function removeCdata() {
		$this->locFile = str_replace('<![CDATA[', '', $this->locFile);
		$this->locFile = str_replace(']]>', '', $this->locFile);
	}

	private function processGeocache(\SimpleXMLElement $geocache) {
		$array['id'] = (string) $geocache->name['id'];
		$array['name'] = (string) $geocache->name;
		$array['lat'] = (float) $geocache->coord['lat'];
		$array['long'] = (float) $geocache->coord['lon'];
		$array['geocaching_countries_id'] = (string) $this->getCountry($array['lat'], $array['long']);
		$array['region'] = (string) $this->getRegion($array['lat'], $array['long']);
		$array['district'] = (string) $this->getDistrict($array['lat'], $array['long']);
		$array['town'] = (string) $this->getTown($array['lat'], $array['long']);
		$array['altitude'] = (int) $this->getGeo($array['lat'], $array['long'])['altitude'];
		//$array['link'] = (string)$geocache->link;
		//$array['added'] = time();

		$name_bp = $array['name'];
		$by_occurance = strpos($array['name'], 'by');
		$array['name'] = trim(substr($name_bp, 0, $by_occurance));
		$array['owner'] = trim(substr($name_bp, $by_occurance + 2));

		$html = file_get_contents('http://www.geocaching.com/seek/cache_details.aspx?wp=' . $array['id']);
		$cacheDetail = $this->getHTMLByClass('cacheDetails', $html);

		$img_start = (strpos($cacheDetail, '<img'));
		$img = substr($cacheDetail, $img_start);
		$img_stop = strpos($img, '/>');
		$img_new = substr($img, 0, $img_stop);

		$img_new_start = strpos($img, 'title="');
		$cache_type_unclear = substr($img_new, $img_new_start);
		$cache_type_clear = str_replace('title="', '', $cache_type_unclear);
		$cache_type_clear = str_replace('"', '', $cache_type_clear);

		$array['geocaching_types_id'] = trim($cache_type_clear);

		return $array;
	}

	private function getGeo($lat, $lon) {
		$geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lon . '&sensor=false&language=sk');

		$geoData = json_decode($geocode, true);

		$vyska_json = file_get_contents('https://maps.google.com/maps/api/elevation/json?locations=' . $lat . ',' . $lon . '&sensor=false&key=AIzaSyBrOTCVwNR3CCWgJflwuKSnuB60GJHU3hQ');

		$geoData['altitude'] = (float) json_decode($vyska_json, true)['results'][0]['elevation'];

		return $geoData;
	}

	public function getRegion($lat, $lon) {
		$geo = $this->getGeo($lat, $lon);

		if (isset($geo['results'][0])) {
			foreach ($geo['results'][0]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_1', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][1])) {
			foreach ($geo['results'][1]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_1', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}
		if (isset($geo['results'][2])) {
			foreach ($geo['results'][2]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_1', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][3])) {
			foreach ($geo['results'][3]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_1', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		return NULL;
	}

	public function getDistrict($lat, $lon) {
		$geo = $this->getGeo($lat, $lon);

		if (isset($geo['results'][0])) {
			foreach ($geo['results'][0]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_2', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][1])) {
			foreach ($geo['results'][1]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_2', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][2])) {
			foreach ($geo['results'][2]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_2', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][3])) {
			foreach ($geo['results'][3]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_2', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][4])) {
			foreach ($geo['results'][4]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_2', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][5])) {
			foreach ($geo['results'][5]['address_components'] as $addressComponet) {
				if (in_array('administrative_area_level_2', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}
		return NULL;
	}

	public function getTown($lat, $lon) {
		$geo = $this->getGeo($lat, $lon);

		if (isset($geo['results'][0])) {
			foreach ($geo['results'][0]['address_components'] as $addressComponet) {
				if (in_array('locality', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}

		if (isset($geo['results'][1])) {
			foreach ($geo['results'][1]['address_components'] as $addressComponet) {
				if (in_array('locality', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		} if (isset($geo['results'][2])) {
			foreach ($geo['results'][2]['address_components'] as $addressComponet) {
				if (in_array('locality', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}
		if (isset($geo['results'][3])) {
			foreach ($geo['results'][3]['address_components'] as $addressComponet) {
				if (in_array('locality', $addressComponet['types'])) {
					return $addressComponet['long_name'];
				}
			}
		}
		return NULL;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/template.latte');

		$this->template->render();
	}

}
