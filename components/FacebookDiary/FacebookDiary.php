<?php

namespace Components;

use Nette\Forms\Form,
	Nette\Utils\Image;

/**
 * Description of FacebookDiary
 *
 * @author Administrator
 */
class FacebookDiary extends BaseControl {

	/** @var \Kdyby\Facebook\Facebook */
	private $facebook;

	/** @var \Nette\Http\Request */
	private $httpRequest;

	/** @var App\Diary */
	private $diary;
	private $showForm = FALSE;

	/** @var array */
	public $onSuccess = [];

	/** @var array */
	public $onFacebookSuccess = [];

	/** @var array */
	public $onFacebookError = [];

	public function setDiary($diary) {
		$this->diary = $diary;
	}

	function __construct(\Kdyby\Facebook\Facebook $facebook, \Nette\Http\Request $httpRequest) {
		$this->facebook = $facebook;
		$this->httpRequest = $httpRequest;
	}

	public function createComponentForm() {
		$form = $this->formFactory->create();

		$diary = $this->diary;

		if ($diary !== NULL) {

			if (!is_null($diary->photogallery)) {

				$gallery = unserialize($diary->photogallery);

				$form_gallery = array_map(function($obj) {
					return \Nette\Utils\Html::el()->setHtml('<img src="' . $obj['thumb'] . '">');
				}, $gallery);

				$form_gallery = array_combine($form_gallery, $form_gallery);

				$form
						->addRadioList('image', 'Obrázok:', $form_gallery)
						->getSeparatorPrototype()->setName(NULL)
				;
			}

			$form
					->addHidden('diary', \Nette\Utils\Json::encode($diary));

			$form
					->addSubmit('fb_send', 'Odoslať')
					->setHtmlId("fb_submit_btn")
			;
		}
		$form->onSuccess[] = [$this, 'onSuccess'];

		return $form;
	}

	public function onSuccess($form) {

		$values = $form->getHttpData();

		$photo = array_key_exists('image', $values) ? $values['image'] : NULL;
		$diary = $values['diary'];

		if ($photo !== NULL) {
			$photo_harvest = str_replace("<img src=", "", rtrim($photo, ">\""));
			$this->sendFacebook($photo_harvest, $diary);
		}else{
			$this->sendFacebook(NULL, $diary);
		}
	}

	public function showForm() {
		$this->showForm = TRUE;
	}

	private function sendFacebook($photo = NULL, $diary = NULL) {
		$photo = ltrim($photo, "\"");
		$url = $this->httpRequest->getUrl();

		$diary = \Nette\Utils\Json::decode($diary);
		$date_from = $diary->fromDate->date;
		$date_from_str = date("d.m.Y",strtotime($date_from));

		if (is_object($diary->toDate)){
			$date_to = $diary->toDate->date;
			$date_to_str = " - ".date("d.m.Y",strtotime($date_to));
		}else {
			$date_to_str = "";
		}

		$photo_url = $photo != NULL ? ($url->scheme . '://' . $url->host . $photo) : NULL;
    //replace thumb
    $photo_url = str_replace("_thumb","",$photo_url);

		$params = [
			"access_token" => "EAADPjpmAWccBAMIG3QpU4yywSxDTTZAWmJ7oyVyqFaF7SXi1J3lNdrXNQVizZCmZCbWkq0CmZAiI8HdRMe5c5eg82uUw3mvDYBXk1lY41zrvu1hl9ksaAnXZCMOWjlijP3HqJP0XwCrs3j4WLFaZB8qP6ZBNgEN8uWMJfEbZB5oZCVwZDZD",
			"message" => $date_from_str.$date_to_str. " - ".$diary->title,
			"link" => $url->scheme . '://' . $url->host.'/diaries/show/'.$diary->id,
			"picture" => isset($photo_url) ? $photo_url : NULL,
			"name" => $diary->title,
			"description" => $date_from_str.$date_to_str. " - ".$diary->title
		];


		if (is_null($photo_url)) {
			unset($params['picture']);
		}

		try {
			$this->facebook->api('/me/feed', 'POST', $params);
			$this->fireCallbacks($this->onFacebookSuccess);
		} catch (\Kdyby\Facebook\FacebookApiException $e) {
			$this->fireCallbacks($this->onFacebookError,[$e]);
		}
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->showForm = $this->showForm;
		$this->template->render();
	}

}
