<?php

namespace components;

use Nette\Application\UI\Control;

/**
 * Description of MapBox
 *
 * @author Administrator
 */
class MapBox extends Control{

    public function __construct() {

    }

    public function render()
    {
        $template = $this->template;

        $template->setFile(__DIR__ . '/mapBox.latte');

        $template->render();
    }
}
