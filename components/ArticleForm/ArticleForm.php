<?php

/**
 * Description of ArticleFormFactory
 *
 * @author Administrator
 */

namespace Components;

use Nette\Forms\Form;

class ArticleForm extends BaseControl {

	const EDIT_MODE = 'edit';
	const INSERT_MODE = 'insert';
	const SAVED = 'saved';

	/** @var int */
	private $mode;
	private $publish = FALSE;

	/** @var \App\Articles * */
	private $article;
	private $introPhoto;

	/** @var Factories\IIntroPhotoUploaderFactory @inject */
	public $introPhotoUploaderFactory;

	/** @var \Dates\Dates @inject */
	public $dates;

	/** @var \App\Model\ArticleModel @inject */
	public $articleModel;

	/** @var array */
	public $onError = [];

	/** @var array */
	public $onSuccess = [];

	/** @var array */
	//public $onFileUploadError = [];

	/** @var array */
	public $onArticleSaved = [];

	/** @var array */
	public $onArticlePublished = [];

	public function createComponentForm() {
		$form = $this->formFactory->create();

		$form
				->addText('title', 'Názov: ')
				->addRule(Form::FILLED, 'Vyplň to')
		;

		$form
				->addUpload('introPhoto')
		;

		if ($this->introPhoto) {
			$form['introPhoto']
					->addRule(Form::FILLED)
					->addRule(Form::IMAGE)
			;
		}

		$form
				->addTextArea('introText', '')
				->setAttribute('class', 'mceEditorIntro')
		;

		$form
				->addText('access_date', 'Dátum sprístupnenia:')
				->addCondition(Form::FILLED)
				->addRule(function($control) {
					$value = $control->value;
					return $this->dates->validateDate(\Dates\Dates::SK, $value);
				}, 'Neplatný dátum')
		;

		$form
				->addText('meta_keywords', 'Meta kľúčové slová')
		;

		$form
				->addTextArea('meta_description', 'Meta popis')
		;

		$form
				->addTextArea('text', '')
				->setAttribute('class', 'mceEditor')
		;

		$form->addSubmit('btnSubmit', 'Uložiť');

		if ($this->mode == self::SAVED) {
			$publishButton = $form->addSubmit('btnPublish', 'Publikovať');
		} else {
			$publishButton = FALSE;
		}

		//$form->onSuccess[] = $this->processForm;

		$form->onSuccess[] = function (Form $form) use ($publishButton) {
			if ($form->isSubmitted() === $publishButton) {
				$this->publish = TRUE;
			}

			$this->processForm($form);
		};

		if ($form->hasErrors()) {
			$this->fireCallbacks($this->onError, [$form->errors]);
		}

		if (!is_null($this->article)) {
			$defaults = $this->prepareFormValues();
		}

		if ($this->mode == DiaryForm::EDIT_MODE) {
			$form->setDefaults($defaults);
		}

		if (!is_null($this->article)) {
			$defaults = $this->prepareFormValues();
		}

		if ($this->mode == ArticleForm::EDIT_MODE || $this->mode == ArticleForm::SAVED) {
			$form->setDefaults($defaults);
			$this->introPhoto = $this->article->introPhoto ? TRUE : FALSE;
		} else {
			$this->introPhoto = FALSE;
		}

		return $form;
	}

	public function processForm($form) {
		$uploadRoot = $_SERVER['DOCUMENT_ROOT'];
		$uploadInside = '/www/uploads/articles';

		$values = $form->getValues();

		$article = $this->mode == self::INSERT_MODE ? new \App\Articles() : $this->article;

		//PUBLIKOVANIE
		if ($this->mode == self::SAVED && $this->publish === TRUE) {

			$this->articleModel->publish($this->article);

			$this->fireCallbacks($this->onArticlePublished);
		}

		$article->title = $values->title;
		$article->introText = $values->introText;
		$article->text = $values->text;
		$article->accessDate = empty($values->access_date) ? null : \DateTime::createFromFormat(\Dates\Dates::SK, $values->access_date);
		$article->metaDescription = $values->meta_description;
		$article->metaKeywords = $values->meta_keywords;

		if (!$this->introPhoto) {
			$introPhoto = $values->introPhoto;
		}

		//ulozenie clanku
		if ($this->mode == self::INSERT_MODE) {
			$inserted_id = $this->articleModel->insert($article);
		} else {
			$inserted_id = $this->article->id;
			$this->articleModel->update($article);
		}

		//Upload intro obrazku
		if (!$this->introPhoto && !is_null($introPhoto->name)) {
			if ($introPhoto->isOk()) {
				$uploadFolder = $uploadRoot . $uploadInside . '/' . $inserted_id . '/' . $introPhoto->getName();

				$introPhoto->move($uploadFolder);

				$article->introPhoto = $introPhoto->getName();
			}
		}

		// Presun obrazkov z uploads/new do zlozky s id
		if ($this->mode == ArticleForm::INSERT_MODE) {
			$text = '<div id="article">' . $values->text . '</div>';
			$dom = new \DOMDocument();
			$dom->loadHTML($text);
			$dom->preserveWhiteSpace = false;
			$images = $dom->getElementsByTagName('img');
			foreach ($images as $image) {
				//$default_old = $image->getAttribute('src');
				$old_src = substr($image->getAttribute('src'), 8); //hack
				$old_name = pathinfo($old_src)['basename'];
				//dump($name);
				$uimage = \Nette\Utils\Image::fromFile($uploadRoot . '/www' . $old_src);
				$uimage->save($uploadRoot . $uploadInside . '/' . $inserted_id . '/' . $old_name);

				$image->setAttribute('src', $uploadInside . '/' . $inserted_id . '/' . $old_name);

				unlink($uploadRoot . '/www' . $old_src);
			}

			$text = $dom->saveHTML();

			$start = strpos($text, '<div id="article">');
			$new_text = substr($text, $start);
			//$end = strpos($new_text, '<end>');

			$new_text = substr($new_text, 18);
			$new_length = strlen($new_text);
			$new_text = substr($new_text, 0, $new_length - 21);
		}

		if ($this->mode == self::INSERT_MODE) {
			$article->text = $new_text;
		} else {
			$article->text = $values->text;
		}

		//update new text value and intro photo
		$this->articleModel->update($article);

		$message = $this->mode == self::EDIT_MODE ? 'Článok bol úspešne aktualizovaný' : 'Článok bol úspešne uložený';

		$this->fireCallbacks($this->onArticleSaved, [$message, $article]);
	}

	public function createComponentIntroPhotoUploader() {
		$form = $this->introPhotoUploaderFactory->create();

		return $form;

		/* $form = $this->formFactory->create();

		  $form->addUpload('introImg', NULL)
		  ->setRequired('File is required')
		  ->addRule(Form::MAX_FILE_SIZE, 'Maximálna povolená veľkosť je 2MB', 2 * 1024 * 1024)
		  ;

		  return $form; */
	}

	public function prepareFormValues() {

		$article = $this->article;
		$article instanceof \App\Articles;

		$defaults = [
			'title' => $article->title,
			'introPhoto' => $article->introPhoto,
			'introText' => $article->introText,
			'access_date' => $this->dates->toFormatedString($article->accessDate, \Dates\Dates::SK),
			'text' => $article->text,
			'meta_description' => $article->metaDescription,
			'meta_keywords' => $article->metaKeywords,
		];

		return $defaults;
	}

	public function setArticle($article) {
		$this->article = $article;
		return $this;
	}

	public function setMode($mode) {
		$this->mode = $mode;
		return $this;
	}

	function render() {
		$this->template->setFile(__DIR__ . '/template.latte');

		if ($this->article && $this->article->introPhoto) {
			$this->template->introPhoto = '/www/uploads/articles/' . $this->article->id . '/' . $this->article->introPhoto;
		} else {
			$this->template->introPhoto = NULL;
		}

		if ($this->mode == self::EDIT_MODE || $this->mode == self::SAVED) {
			$this->template->id = $this->article->id;
		} else {
			$this->template->id = null;
		}

		$this->template->saved = $this->mode == self::SAVED ? TRUE : FALSE;

		$this->template->render();
	}

	public function handleremoveIntro() {
		$this->articleModel->removeIntro($this->article);
		$this->introPhoto = FALSE;
		$this->redirect('this');
	}

	function getMode() {
		return $this->mode;
	}

}
