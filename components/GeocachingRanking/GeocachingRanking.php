<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Components;

use Nette\Utils\Html;

/**
 * Description of FullRankingTable
 *
 * @author Administrator
 */
class GeocachingRanking extends \Components\BaseControl {

    /** @var \Geocaching\GeocachingLocationsTypes @inject */
    public $locationTypes;

    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /** @var Geocaching\GeocachingLocationsTypes */
    private $actualType;

    public function __construct() {
	$this->actualType = \Geocaching\GeocachingLocationsTypes::COUNTRY_RANKING;
    }

    public function setEm($em) {
	$this->em = $em;
    }

    public function handleType($type) {

	$type = \strtolower($type);

	try {
	    if (!in_array($type, $this->locationTypes->getAllowedTypes())) {
		throw new \MVException\NotAllowedGeocachingLocationsType();
	    }
	} catch (\MVException\NotAllowedGeocachingLocationsType $e) {
	    $this->flashMessage('Nepovolená akcia', 'error');
	    $this->redirect('this');
	}

	$this->actualType = $type;
    }

    protected function createComponentCountryRankingGrid() {
	$grid = new \Grido\Grid();

	$basePath = $this->template->basePath;

	$model = new \Grido\DataSources\Doctrine(
		//$this->em->createNativeQuery('SELECT id FROM geocaching g JOIN geocaching_countries gc ON (gc.id = g.country)', $rsm)
		$this->em->createQueryBuilder()
			->select('COUNT(gc) AS county,g')
			->from('\App\Geocaching', 'g')
			->leftJoin('g.country', 'gc')
			->groupBy('gc.id')
			->orderBy('county', 'DESC')
		)
	;

	$grid->model = $model;

	$grid->addColumnText('country', 'Krajina')
			->setCustomRender(function($item) use ($basePath) {
			    $img = Html::el('img')->src("$basePath/images/flags/shiny/16/" . $item->country->image);
			    $img->title($item->country->country);
			    return $img . ' ' . $item->country->country;
			})->headerPrototype->style[] = "width:80%; text-align:left;";

	$grid->addColumnNumber('id', 'Počet')
		->setCustomRender(function($item) use ($basePath) {
		    $count = $this->em->createQuery('SELECT COUNT(g.id) FROM App\Geocaching g WHERE g.country = :country')
			    ->setParameter('country', $item->country->id);
		    $res = $count->getSingleScalarResult();
		    return $res;
		}
	);

	$grid->filterRenderType = \Grido\Components\Filters\Filter::RENDER_INNER;

	return $grid;
    }

    protected function createComponentRegionRankingGrid() {
	$grid = new \Grido\Grid();

	$basePath = $this->template->basePath;

	$grid->model = $this->getRegionData();

	$grid->addColumnText('country', 'Krajina')
			->setCustomRender(function($item) use ($basePath) {
			    $img = Html::el('img')->src("$basePath/images/flags/shiny/16/" . $item['image']);
			    $img->title($item['country']);
			    return $img . ' ' . $item['country'];
			})->headerPrototype->style[] = "width:20%; text-align:left;";
	;

	$grid->addColumnText('region', 'Kraj')
		->headerPrototype->style[] = "width:70%;";

	$grid->addColumnText('count', 'Počet')
		->headerPrototype->style[] = "width:10%";


	$grid->filterRenderType = \Grido\Components\Filters\Filter::RENDER_INNER;

	return $grid;
    }

    protected function createComponentDistrictRankingGrid() {
	$grid = new \Grido\Grid();

	$basePath = $this->template->basePath;

	$grid->model = $this->getDistrictData();

	$grid->addColumnText('country', 'Krajina')
			->setCustomRender(function($item) use ($basePath) {
			    $img = Html::el('img')->src("$basePath/images/flags/shiny/16/" . $item['image']);
			    $img->title($item['country']);
			    return $img . ' ' . $item['country'];
			})
		->headerPrototype->style[] = "width:20%; text-align:left;"
	;

	$grid->addColumnText('district', 'Okres')
		->headerPrototype->style[] = "width:50%;"
	;

	$grid->addColumnText('region', 'Kraj')
		->headerPrototype->style[] = "width:20%;"
	;

	$grid->addColumnText('count', 'Počet')
		->headerPrototype->style[] = "width:10%;"
	;


	$grid->filterRenderType = \Grido\Components\Filters\Filter::RENDER_INNER;

	return $grid;
    }

    protected function createComponentTownRankingGrid() {
	$grid = new \Grido\Grid();

	$basePath = $this->template->basePath;

	$grid->model = $this->getTownData();

	$grid->addColumnText('country', 'Krajina')
			->setCustomRender(function($item) use ($basePath) {
			    $img = Html::el('img')->src("$basePath/images/flags/shiny/16/" . $item['image']);
			    $img->title($item['country']);
			    return $img . ' ' . $item['country'];
			})
		->headerPrototype->style[] = "width:20%;"
	;

	$grid->addColumnText('town', 'Obec')
		->headerPrototype->style[] = "width:50%;"
	;

	$grid->addColumnText('district', 'Okres')
		->headerPrototype->style[] = "width:20%;"
	;


	$grid->addColumnText('count', 'Počet')
		->headerPrototype->style[] = "width:10%;"
	;

	$grid->filterRenderType = \Grido\Components\Filters\Filter::RENDER_INNER;

	return $grid;
    }

    private function getRegionData() {
	$query = $this->em->createQuery('SELECT g, COUNT(g) AS county FROM App\Geocaching g GROUP BY g.region ORDER BY county DESC');

	$data = $query->getResult();

	$array = [];
	$it = 0;
	foreach ($data as $gCache) {
	    $array [$it]['image'] = $gCache[0]->country->image;
	    $array [$it]['country'] = $gCache[0]->country->country;
	    $array [$it]['region'] = $gCache[0]->region;
	    $array [$it]['count'] = $gCache['county'];
	    $it++;
	}

	return $array;
    }

    private function getDistrictData() {
	$query = $this->em->createQuery('SELECT g, COUNT(g) AS county FROM App\Geocaching g GROUP BY g.district ORDER BY county DESC');

	$data = $query->getResult();

	$array = [];
	$it = 0;
	foreach ($data as $gCache) {
	    $array [$it]['image'] = $gCache[0]->country->image;
	    $array [$it]['country'] = $gCache[0]->country->country;
	    $array [$it]['region'] = $gCache[0]->region;
	    $array [$it]['district'] = $gCache[0]->district;
	    $array [$it]['count'] = $gCache['county'];
	    $it++;
	}

	return $array;
    }

    private function getTownData() {
	$query = $this->em->createQuery('SELECT g, COUNT(g) AS county FROM App\Geocaching g GROUP BY g.town ORDER BY county DESC');

	$data = $query->getResult();

	$array = [];
	$it = 0;
	foreach ($data as $gCache) {
	    $array [$it]['image'] = $gCache[0]->country->image;
	    $array [$it]['country'] = $gCache[0]->country->country;
	    $array [$it]['region'] = $gCache[0]->region;
	    $array [$it]['district'] = $gCache[0]->district;
	    $array [$it]['town'] = $gCache[0]->town;
	    $array [$it]['count'] = $gCache['county'];
	    $it++;
	}

	return $array;
    }

    public function render() {
	$this->template->actualType = $this->actualType;
	$this->template->locationTypes = $this->locationTypes->getTypes();
	$this->template->setFile(__DIR__ . '/template.latte');
	$this->template->render();
    }

}
