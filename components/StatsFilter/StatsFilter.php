<?php

namespace Components;
/**
 * Description of StatsFilter
 *
 * @author Administrator
 */
class StatsFilter extends BaseControl {

	/** @var \Nette\Http\SessionSection */
	private $sessionSection;

	/** @var \Nette\Http\Session */
	private $session;
	
	/** @var \DateTime */
	private $yearStart;

	/** @var \DateTime */
	private $yearEnd;

	/** @var array */
	private $periods;
	
	/** @persistent */
	public $from;
	
	/** @persistent */
	public $to;

	/** @persistent @var int */
	public $period;
	
	private $defaultPeriod = 8;

	public function __construct(\Nette\Http\Session $session) {
		$this->session = $session;
		$this->yearStart = $this->dateBoundary('first day of January');
		$this->yearEnd = $this->dateBoundary('last day of December');

		$lastYear = (new \DateTime('last year'))->format('Y');
		
		$this->periods = array(
			1	 => [
				'label'	 => 'Tento mesiac',
				'from'	 => $this->dateBoundary('first day of this month'),
				'to'	 => $this->dateBoundary('last day of this month')
			],
			2	 => [
				'label'	 => 'Minulý mesiac',
				'from'	 => $this->dateBoundary('first day of last month'),
				'to'	 => $this->dateBoundary('last day of last month')
			],
			3	 => [
				'label'	 => '1. kvartál',
				'from'	 => $this->dateBoundary('first day of January'),
				'to'	 => $this->dateBoundary('last day of March')
			],
			4	 => [
				'label'	 => '2. kvartál',
				'from'	 => $this->dateBoundary('first day of April'),
				'to'	 => $this->dateBoundary('last day of June')
			],
			5	 => [
				'label'	 => '3. kvartál',
				'from'	 => $this->dateBoundary('first day of July'),
				'to'	 => $this->dateBoundary('last day of September')
			],
			6	 => [
				'label'	 => '4. kvartál',
				'from'	 => $this->dateBoundary('first day of October'),
				'to'	 => $this->dateBoundary('last day of December')
			],
			9	 => [
				'label'	 => 'Tento rok',
				'from'	 => $this->yearStart,
				'to'	 => $this->yearEnd
			],
			7	 => [
				'label'	 => 'Predošlý rok',
				'from'	 => $this->dateBoundary(sprintf('%s-01-01', $lastYear)),
				'to'	 => $this->dateBoundary(sprintf('%s-12-31', $lastYear))
			],
			8	 => [
				'label'	 => 'Všetko',
				'from'	 => NULL,
				'to'	 => NULL
			],
		);
	}
	
	public function setSessionNamespace($namespace)
	{
		$this->sessionSection = $this->session->getSection($namespace);
		return $this;
	}
			
	
	private function dateBoundary($dateString, $format = 'd.m.Y')
	{
		$dt = new \DateTime($dateString);
		return $dt->format($format);
	}

	public function getFrom()
	{
		$dt = $this->getPeriodDate('from');
		if (is_null($dt)) {
			return NULL;
		}
		$dt->setTime(0, 0, 0);
		return $dt;
	}

	public function getTo()
	{
		$dt = $this->getPeriodDate('to');
		if (is_null($dt)) {
			return NULL;
		}
		$dt->setTime(23, 59, 59);
		return $dt;
	}
	
	private function getPeriodDate($filter)
	{
		
		if (empty($this->period)) {

			$period = $this->sessionSection->offsetGet('period');

			if (empty($period)) {
				$index = $this->defaultPeriod;
			} else {
				$index = $period;
			}
		} else {
			$index = $this->period;
		}

		$period = $this->periods[(int) $index];

		$date = $period[$filter];
		if (is_null($date)) {
			return NULL;
		}
		return new \DateTime($date);
	}

	public function getPeriod()
	{
		return $this->period;
	}
	
	public function render()
	{
		if (is_null($this->period)) {

			if (is_null($this->sessionSection->offsetGet('period'))) {
				$this->period = $this->defaultPeriod;
				$this->sessionSection->offsetSet('period', $this->period);
			}

			$this->period = $this->sessionSection->offsetGet('period');
		} else {
			$this->sessionSection->offsetSet('period', $this->period);
		}

		$this->template->periods		 = $this->periods;
		$this->template->selectedPeriod	 = $this->periods[$this->period];

		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->render();
	}

}
