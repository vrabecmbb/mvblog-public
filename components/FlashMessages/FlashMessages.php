<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace components;

use Nette\Application\UI\Control;
/**
 * Description of FlashMessages
 *
 * @author Administrator
 */
class FlashMessages extends Control{
    
    public function render() {
        $this->template->setFile(__DIR__.'/flashes.latte');
        $this->template->flashes = $this->presenter->getTemplate()->flashes;
        $this->render();
    }
    
}
