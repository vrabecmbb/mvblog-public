<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CalendarGenerator
 *
 * @author Administrator
 */
class CalendarGenerator extends blitzik\Calendar\Generator\CalendarGenerator {
	
	
	
	protected function generateCalendar($year, $month)
    {
        $this->cellFactory->setPeriod($year, $month);
        $calendarTable = [];
        for ($row = 0; $row < $this->cellFactory->getNumberOfRows(); $row++) {
            for ($col = 0; $col < $this->cellFactory->getNumberOfColumns(); $col++) {
                $cell = $this->cellFactory->createCell($row, $col);
                $calendarTable[$cell->getNumber()] = $cell;
            }
        }
		//dump($calendarTable);
        return $calendarTable;
    }
	
	
}
