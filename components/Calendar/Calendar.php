<?php

/**
 * Description of ArticleFormFactory
 *
 * @author Administrator
 */

namespace Components;

class Calendar extends \blitzik\Calendar\Calendar {
	
	protected $numberOfDaysLabelsCharactersToTruncate;
	protected $areSelectionsActive = FALSE;
	
	/** @var \GeocachingManager **/
	private $geocachingManager;
	
	/** @var \App\Model\DiaryModel **/
	private $diaryModel;
	
	/** @var \App\Model\ArticleModel **/
	private $articleModel;
	
	/** @var array **/
	private $colors;
	
	/*public function __construct(\GeocachingManager $geocahingManager) {
		parent::__construct('sk');
		$this->numberOfDaysLabelsCharactersToTruncate = 2;
		$this->geocachingManager = $geocahingManager;
	}*/
	
 function __construct($colors, \GeocachingManager $geocachingManager, \App\Model\DiaryModel $diaryModel, \App\Model\ArticleModel $articleModel) {
	 parent::__construct('sk');
	 $this->numberOfDaysLabelsCharactersToTruncate = 2;
	 $this->geocachingManager = $geocachingManager;
	 $this->diaryModel = $diaryModel;
	 $this->articleModel = $articleModel;
	 $this->colors = $colors;
 }
	

	public function getMonthName($monthNumber, $type = NULL)
    {
        \Nette\Utils\Validators::assert($monthNumber, 'numericint:1..12');
		
		$format = is_null($type) ? 'F' : 'M';
		
        return strtolower(\DateTime::createFromFormat('!m', $monthNumber)->format($format));
    }
	
	private function prepareColor($geocaches, $diaries, $articles){
		if (count($articles) > 0){
			return $this->colors['articles'];
		} elseif (count($geocaches) > 0){
			return $this->colors['geocaching'];
		} elseif (count($diaries) > 0){
			return $this->colors['diary'];
		}
	}
	
	public function prepareCell($row, $col){
		$cellNumber =  $this->cellFactory->calcNumber($row, $col);
		$day = $this->calendarData[$cellNumber]->getDay();
		$day->geocaches = $this->geocachingManager->getByDate($day->getDatetime());
		$day->diaries = $this->diaryModel->getByDate($day->getDatetime());
		$day->articles = $this->articleModel->getByDate($day->getDatetime());
		
		$day->color = $this->prepareColor($day->geocaches, $day->diaries, $day->articles );
		return $this->calendarData[$cellNumber];
	}
	
	private function  prepareLinks(){
		$diaries = $this->diaryModel->getByTime($this->month, $this->year);
		$geocaches = $this->geocachingManager->getByFoundMonth($this->month, $this->year);
		$articles = $this->articleModel->getByMonth($this->month, $this->year);
		
		return [
			'diaries' => $diaries,
			'geocaches' => $geocaches,
			'articles' => $articles, 
			'colors' => $this->colors
		];
	}


	public function render()
    {
        $template = $this->getTemplate();
        $template->setFile(__DIR__ . '/calendar.latte');

        if (isset($this->translator)) {
            $template->setTranslator($this->translator);
        }

        if (!isset($this->calendarGenerator)) {
            $this->cellFactory = new HorizontalCalendarCellFactory();
            $this->calendarGenerator = new CalendarGenerator($this->cellFactory);
        }

        $this->calendarData = $this->getCalendarData();
        $template->month = $this->month;
        $template->year = $this->year;
        $template->areSelectionsActive = TRUE;
        $template->charsToShortTo = $this->numberOfDaysLabelsCharactersToTruncate;

        $template->rows = $this->cellFactory->getNumberOfRows();
        $template->cols = $this->cellFactory->getNumberOfColumns();

		$template->getCell = function($row, $col){
			return $this->prepareCell($row, $col);
		};

        $template->getMonthName = function ($monthNumber) {
            return $this->getMonthName($monthNumber);
        };

		$template->getMonthNameSelect = function ($monthNumber) {
            return $this->getMonthName($monthNumber, true);
        };
		
		$template->links = $this->prepareLinks();
		
        $template->calendarBlocksTemplate = __DIR__ . '/calendarBlocks.latte';

        $template->render();
    }
	
}