<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CalendarDay
 *
 * @author Administrator
 */
class CalendarDay extends blitzik\Calendar\Entities\Day{
	
	/** @var ICell */
    private $cell;
	
	/** var array */
	public $caches;
	
	/** var array */
	public $diaries;
	
	/** var array */
	public $articles;
	
	/** @var GeocachingManager **/
	private $geocachingManager;
	
	public $color;
	
	
	public function __construct(\blitzik\Calendar\Entities\ICell $cell)
    {
		parent::__construct($cell);
        /*$this->cell = $cell;
        $this->date = $this->prepareDateForDay($cell);*/
		$this->geocaches = [];
		$this->diaries = [];
		$this->articles = [];
		$this->color;
    }
	
	/**
     * @param ICell $cell
     * @return \DateTime
     */
    private function prepareDateForDay(blitzik\Calendar\Entities\ICell $cell)
    {
        $d = \DateTime::createFromFormat('!Y-m', $cell->getYear()."-".$cell->getMonth());
	
        if ($cell->getNumber() <= 0) {
            $days = abs($cell->getNumber() - 1);
            $d->sub(new \DateInterval('P'.$days.'D'));

        } elseif ($cell->getNumber() > $this->cell->getNumberOfDaysInMonth()) {
            $days = $cell->getNumber() - 1;
            $d->add(new \DateInterval('P'.$days.'D'));

        } else { // 0 < $cellNumber <= $numberOfDays
            $days = $cell->getNumber();
            $d = new \DateTime($cell->getYear().'-'.$cell->getMonth().'-'.$days);
        }

        return $d;
    }
	
	function setGeocaches($geocaches) {
		$this->geocaches = $geocaches;
	}

	function setDiaries($diaries) {
		$this->diaries = $diaries;
	}

	function setArticles($articles) {
		$this->articles = $articles;
	}


	
}
