<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CalendarCellFactory
 *
 * @author Administrator
 */
abstract class CalendarCellFactory extends \blitzik\Calendar\Factories\CellFactory implements ICellFactory{
	
	private $geocaches;
	
	public function createCell($row, $col)
    {
        return new CalendarCell(
            $this->calcNumber($row, $col),
            $this->year,
            $this->month,
            $this->isForDayLabel($row, $col)
        );
    }
}
