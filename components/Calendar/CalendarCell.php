<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CalendarCell
 *
 * @author Administrator
 */
class CalendarCell extends blitzik\Calendar\Entities\Cell{
	
	/*public function __construct($cellNumber, $year, $month, $isForLabel)
    {
        $this->number = $cellNumber;
        $this->year = (int) $year;
        $this->month = (int) $month;
        $this->isLabelCell = $isForLabel;
        $this->numberOfDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }*/
	
	/*/**
     * @return IDay
     */
    /*public function getDay()
    {
        if (!isset($this->day)) {
            $this->day = $this->createDay();
        }
        return $this->day;
    }*/
    /**
     * @return IDay
     */
    protected function createDay()
    {
        return new CalendarDay($this);
    }
}
