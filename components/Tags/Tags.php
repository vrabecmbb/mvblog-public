<?php 

namespace Components;

class Tags extends BaseControl{

    /** @var \App\Model\DiaryModel @inject */
    public $diaryModel;
    
    private $diary;
    
    private $tags;

    public function getDiaryTags($diary_id){
        $toRet = $this->diaryModel->getTags($diary_id);
        $this->tags = $toRet;
        return $toRet;
    }
    
    public function handleRemoveTag($tag_id,$diary_id){
        return $this->diaryModel->removeTag($diary_id, $tag_id);
    }
    
    public function setDiary($diary_id){
        $this->diary = $diary_id;
    }
    
    public function renderFront(){
        $this->template->setFile(__DIR__ . '/template.latte');
        $this->template->tags = $this->tags;
        
        $this->template->render();
    }
    
    public function renderAdmin(){
        $this->template->setFile(__DIR__ . '/template_admin.latte');
        $this->template->tags = $this->tags;
        $this->template->diary = $this->diary;
        
        $this->template->render();
    }
}
?>