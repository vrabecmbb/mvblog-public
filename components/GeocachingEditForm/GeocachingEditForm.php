<?php

namespace Components;

/**
 * Description of GeocachingEditFOrm
 *
 * @author Administrator
 */
class GeocachingEditForm extends BaseControl {

	/** @var \GeocachingManager * */
	private $geocachingManager;

	/** @var array */
	public $onSuccess = [];

	/** @var array */
	public $onError = [];

	function __construct(\GeocachingManager $geocachingManager) {
		$this->geocachingManager = $geocachingManager;
	}

	public function createComponentForm() {
		$form = $this->formFactory->create();
		$form->addText("region", "Región");
		$form->addText("district", "Okres");
		$form->addText("town", "Obec");
		$form->addText('date', 'Dátum nálezu');
		$form->addText('hour', 'Hodina');
		$form->addText('minutes', 'Minúta');
		$form->addHidden('id', NULL);

		$form->addSubmit("save", "Uložiť");

		$form->onSuccess[] = [$this, 'save'];

		return $form;
	}

	public function setDefaults($defaults) {
		$this['form']->setDefaults($defaults);
	}

	public function save($form) {
		$values = $form->getValues();

		//$date = new \DateTimeImmutable;

		$date = [
			'day' => substr($values->date, 0, 2),
			'month' => substr($values->date, 3, 2),
			'year' => substr($values->date, 6),
			'hour' => $values->hour,
			'minutes' => $values->minutes
		];
		
		$time = new \DateTime;
		$time->setDate($date['year'], $date['month'], $date['day']);
		$time->setTime($date['hour'], $date['minutes']);

		$geocache = $this->geocachingManager->getCache($values->id);
		$geocache->region = $values->region;
		$geocache->town = $values->town;
		$geocache->district = $values->district;
		$geocache->found = $time;


		$this->geocachingManager->update();

		$this->fireCallbacks($this->onSuccess, ['Keška ' . $geocache->id . ' ( ' . $geocache->name . ' ) bola aktualizovaná']);
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->editFormTitle = '';
		$this->template->render();
	}

}
