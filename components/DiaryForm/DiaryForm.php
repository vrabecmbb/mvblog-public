<?php

namespace Components;

use Nette\Forms\Form,
	Nette\Utils\Image;

/**
 * Description of DiayFormFactory
 *
 * @author Administrator
 */
class DiaryForm extends BaseControl {

	const EDIT_MODE = 'edit';
	const INSERT_MODE = 'insert';

	/** @var int */
	private $mode;

	/** @var \App\Diary * */
	private $diary;
        
        /** @var Factories\ITagsFactory */
        private $tagsFactory;

	/** @var \Dates\Dates @inject */
	public $dates;

	/** @var \App\Model\DiaryModel @inject */
	public $diaryModel;

	/** @var array */
	public $onError = [];

	/** @var array */
	public $onSuccess = [];

	/** @var array */
	public $onFileUploadError = [];

	/** @var array */
	public $onDiarySaved = [];
        
        function __construct(Factories\ITagsFactory $tagsFactory) {
            $this->tagsFactory = $tagsFactory;
        }

        	public function createComponentForm() {
		$form = $this->formFactory->create();

		$form
				->addText('title', 'Názov: ')
				->addRule(Form::FILLED, 'Vyplň to')
		;

		$form
				->addText('from', 'Od:')
				->addRule(Form::FILLED, 'Dátum začiatku udalosti nebol vyplnený')
				->addRule(function($control) {
					$value = $control->value;
					return $this->dates->validateDate(\Dates\Dates::SK, $value);
				}, 'Neplatný dátum')
		;

		$form
				->addText('to', 'Do:')
				->addCondition(Form::FILLED)
				->addRule(function($control) {
					$value = $control->value;
					return $this->dates->validateDate(\Dates\Dates::SK, $value);
				}, 'Neplatný dátum')
		;

		$form
				->addTextArea('text', '')
				->setAttribute('class', 'mceEditor')
		;
        
        $form
            ->addText('tags','Tagy:');

		$form
		->addUpload('fotogaleria', 'Fotogaléria:')
		//->addRule(Form::MIME_TYPE, 'Povolený formát je zip', 'application/zip');
		;

		$form->onValidate[] = function($form) {
			if (isset($form->values['to']) && ( $form['to']->value < $form['from']->value)) {
				$date1 = $form->values['from'];
				$date2 = $form->values['to'];

				if ($date2 && !$this->dates->validateDate($date1, $date2)) {
					$form->addError('Koniec udalosti musí byť neskôr ako začiatok udalosti');
				}
			}
		};

		$form->addSubmit('send', 'Odoslať');

		$form->onSuccess[] = [$this,'processForm'];

		if ($form->hasErrors()) {
			$this->fireCallbacks($this->onError, [$form->errors]);
		}

		if (!is_null($this->diary)) {
			$defaults = $this->prepareFormValues();
		}

		if ($this->mode == DiaryForm::EDIT_MODE){
			$form->setDefaults($defaults);
		}

		return $form;
	}
        
        public function createComponentTags(){
            $tags = $this->tagsFactory->create();
            
            $tags->getDiaryTags($this->diary->id);
            $tags->setDiary($this->diary->id);
            
            return $tags;
        }

	public function processForm($form) {
		$uploadRoot = $_SERVER['DOCUMENT_ROOT'];
		$uploadInside = '/www/uploads/diaries';
		$uploadFolder = $uploadRoot . $uploadInside;

		$values = $form->getValues();

		$fotogaleria = $form['fotogaleria']->getValue();

		unset($values['fotogaleria']);

		$fromDate = \DateTime::createFromFormat('d.m.Y', $values->from);
		$fromDate->setTime(0, 0, 0);

		if (isset($values->to)) {
			$toDate = \DateTime::createFromFormat('d.m.Y', $values->to);
		}

		$diary = $this->mode == self::INSERT_MODE ? new \App\Diary() : $this->diary;

		$diary->title = $values->title;
		$diary->fromDate = $fromDate;
		if (!empty($values->to)) {
			$diary->toDate = $toDate;
		}
		$diary->text = $values->text;
        
        //foreach tags tu bude

        $tag = new \App\Tags();
        $tag->name = $values->tags;
        
        $diary->addTag($tag);
        
        
        // koniec tu budu tags

			//ulozenie diaru
			if ($this->mode == self::INSERT_MODE) {
				$inserted_id = $this->diaryModel->insert($diary);
			} else {
				$this->diaryModel->update($diary);
				$inserted_id = $this->diary->id;
			}

			if(!is_null($fotogaleria->name)){
				if ($fotogaleria->isOk()) {
					//upload zip file to server
					$fotogaleria->move($uploadFolder . '/' . 'last.zip');

					//extrakt zip archivu
					$zip = new \ZipArchive;

					if ($zip->open($fotogaleria->getTemporaryFile(), \ZipArchive::CREATE) === TRUE) {
						$zip->extractTo($uploadFolder . '/' . $inserted_id);
						
						foreach ($this->getFileFromZip($zip) as $file) {
							$image = Image::fromFile($uploadFolder . '/' . $inserted_id . '/' . $file);
							$image->resize(125, NULL, Image::SHRINK_ONLY);

							$fileName = substr($file, 0, strlen($file) - 4);
							$fileType = substr($file, strlen($file) - 3, strlen($file));

							$imagesArray[] = [
								'image' => $uploadInside . '/' . $inserted_id . '/' . $fileName . '.' . $fileType,
								'thumb' => $uploadInside . '/' . $inserted_id . '/' . $fileName . '_thumb.' . $fileType
							];

							$image->save($uploadFolder . '/' . $inserted_id . '/' . $fileName . '_thumb.' . $fileType, 80);
						}
					} else {
						dump('error');
					}

					//update also insert new photogallery
					$this->diaryModel->addPhotogallery($inserted_id, serialize($imagesArray));

				} else {
					$this->fireCallbacks($this->onFileUploadError, [$fotogaleria->getError()]);
				}
			}

			$message = $this->mode == self::EDIT_MODE ? 'Denník bol úspešne aktualizovaný' : 'Denník bol úspešne vložený';
			
			$this->fireCallbacks($this->onDiarySaved, [$message]);
	}

	public function prepareFormValues() {

		$diary = $this->diary;
		$diary instanceof \App\Diary;
                
                dump($diary->getTags());

		$defaults = [
			'title' => $diary->title,
			'from' => $this->dates->toFormatedString($diary->fromDate, \Dates\Dates::SK),
			'to' => $this->dates->toFormatedString($diary->toDate, \Dates\Dates::SK),
			'text' => $diary->text
		];

		return $defaults;
	}

	public function setDiary($diary) {
		$this->diary = $diary;
		return $this;
	}

	public function setMode($mode) {
		$this->mode = $mode;
		return $this;
	}

	private function getFileFromZip($ziparchive) {

		$toRet = [];

		for ($i = 0; $i < $ziparchive->numFiles; $i++) {
			$stat = $ziparchive->statIndex($i);
			//print_r( basename( $stat['name'] ) . PHP_EOL );

			$toRet[] = basename($stat['name']);
		}

		asort($toRet);
		return $toRet;
	}

	private function prepareThumbnails(){

		$gallery = ltrim($this->diary->photogallery,'.');
		$ret = unserialize($gallery);

		return $ret;
	}
        
	function render() {
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->file_input = $this->diary ? true : false;
		if ($this->mode == DiaryForm::EDIT_MODE){
			$this->template->hasPhotogallery = !is_null($this->diary->photogallery) ? true : false;
		}else {
			$this->template->hasPhotogallery = FALSE;
		}

		if ($this->mode == self::EDIT_MODE){
			$this->template->thumbs = $this->prepareThumbnails();
		}

		$this->template->render();
	}

}
