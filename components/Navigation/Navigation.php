<?php

namespace Components;

/**
 * Description of Navigation
 *
 * @author Administrator
 */
class Navigation extends BaseControl{
	
	private $navigation;
	
	public function __construct($navigation) {
		$this->navigation = $navigation;
	}
	
	private function prepareNavigation() {
		
		$navigationsArrays = array_keys($this->navigation);
		
		$it = 0;
		foreach ($this->navigation as $navigations){
			$stringLen = strlen($navigations['presenter']);
			
			$this->navigation[$navigationsArrays[$it]]['pureName'] = substr($navigations['presenter'], 0, $stringLen-1);
			$it++;
		}
		
		$preparedSet = [];
		foreach ($navigationsArrays as $navigation){
			$preparedSet[] = $this->navigation[$navigation];
		}
		
		return $preparedSet;
	}
	
	public function renderMainMenu() {

		$this->template->setFile(__DIR__.'/main-navigation.latte');

		$this->template->mainLogo = $this->presenter->context->parameters['logoImage'];
		$this->template->navigation = $this->prepareNavigation();
		$this->template->presenter = $this->presenter->getPureName();
		
		$this->template->render();
	}
	
	public function renderStickyMenu() {
		$this->template->setFile(__DIR__.'/sticky-navigation.latte');
		
		$this->template->stickyLogo = $this->presenter->context->parameters['logoSmallImage'];
		$this->template->navigation = $this->prepareNavigation();
		$this->template->presenter = $this->presenter->getPureName();
		
		$this->template->render();
	}
	
	public function renderToggleMenu() {
		$this->template->setFile(__DIR__.'/toggle-navigation.latte');
		
		$this->template->stickyLogo = $this->presenter->context->parameters['logoSmallImage'];
		$this->template->navigation = $this->prepareNavigation();
		$this->template->presenter = $this->presenter->getPureName();
		
		$this->template->render();
	}
}
