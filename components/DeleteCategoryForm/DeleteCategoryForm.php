<?php

namespace Components;

use Nette\
Nette\Forms\Form,
    \App\PhotoCategory,
    Nette\Utils\Image;

/**
 * Description of PhotoCategoryForm
 *
 * @author Administrator
 */
class DeleteCategoryForm extends \Components\BaseControl {

    /** @var PhotogalleryManager @inject */
    public $photogalleryManager;

    /** @var \App\PhotoCategory */
    private $photocategory;
    private $id;

    /** @var array */
    public $onDelete = [];

    /** @var array */
    public $onError = [];

    function __construct(\PhotogalleryManager $photogalleryManager) {
        parent::__construct();
        $this->photogalleryManager = $photogalleryManager;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setPhotocategory(\App\PhotoCategory $photocategory) {
        $this->photocategory = $photocategory;
    }

    
    public function createComponentForm() {

        $form = new \Nette\Application\UI\Form();

        $form->addHidden('id')
                ->setDefaultValue($this->id);

        $form->addSubmit('delete');

        $form->onSuccess[] = [$this, 'processForm'];

        $this->onError[] = [$this, 'onError'];

        $form->onError[] = function(\Nette\Application\UI\Form $form) {
            $this->fireCallbacks($this->onError);
        };

        return $form;
    }

    public function processForm(\Nette\Application\UI\Form $form) {
        
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/template.latte');

        $this->template->kategoria = $this->photocategory;

        $this->template->render();
    }

}
