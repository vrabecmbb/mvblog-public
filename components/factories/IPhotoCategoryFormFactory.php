<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IPhotoCategoryFormFactory {
    
    /** @return \Components\PhotoCategoryForm */
    public function create();
}
