<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IIntroPhotoUploaderFactory {
    
    /** @return \Components\IntroPhotoUploader */
    public function create();
}
