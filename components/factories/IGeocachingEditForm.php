<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IGeocachingEditFormFactory {
    
    /** @return \Components\GeocachingEditForm */
    public function create();
}
