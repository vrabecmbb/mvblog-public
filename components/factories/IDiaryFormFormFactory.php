<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IDiaryFormFactory {
    
    /** @return \Components\DiaryForm */
    public function create();
}
