<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IDiarySearchFormFactory{
    
    /** @return \Components\DiarySearchForm */
    public function create();
}
