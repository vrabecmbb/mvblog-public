<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IMailFormFactory {

    /** @return \Components\MailForm */
    public function create();
}
