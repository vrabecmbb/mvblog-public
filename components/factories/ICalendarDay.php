<?php

namespace Components\Factories;

/**
 * Description of ICalendarDay
 *
 * @author Administrator
 */
interface ICalendarDay {
	
	/** @return \CalendarDay */
    public function create();
}
