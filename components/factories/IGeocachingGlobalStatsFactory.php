<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IGeocachingGlobalStatsFactory{
    
    /** @return \Components\GeocachingGlobalStats */
    public function create();
}
