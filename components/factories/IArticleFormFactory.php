<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IArticleFormFactory {
    
    /** @return \Components\ArticleForm */
    public function create();
}
