<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IFacebookDiaryFactory {
    
    /** @return \Components\FacebookDiary */
    public function create();
}
