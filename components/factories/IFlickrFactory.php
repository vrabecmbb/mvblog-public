<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IFlickrFactory{
    
    /** @return \Components\Flickr */
    public function create();
}
