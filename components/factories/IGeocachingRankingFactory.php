<?php

namespace Components\Factories;
/**
 * Description of IGeocachingRankingFactory
 *
 * @author Administrator
 */
interface IGeocachingRankingFactory {
    
    /** @return \Components\GeocachingRanking */
    public function create();
}
