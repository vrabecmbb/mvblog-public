<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IPanoramioFactory {
    
    /** @return \Components\Panoramio */
    public function create();
}
