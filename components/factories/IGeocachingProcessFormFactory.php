<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IGeocachingProcessFormFactory {
    
    /** @return \Components\GeocachingProcessForm */
    public function create();
}
