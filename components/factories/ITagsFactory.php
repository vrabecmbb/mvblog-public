<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface ITagsFactory {
    
    /** @return \Components\Tags */
    public function create();
}
                   