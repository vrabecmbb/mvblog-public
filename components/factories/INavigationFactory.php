<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface INavigationFactory {
    
    /** @return \Components\Navigation */
    public function create();
}
