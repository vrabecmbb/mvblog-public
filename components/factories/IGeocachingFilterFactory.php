<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IGeocachingFilterFactory {
    
    /** @return \Components\GeocachingFilter */
    public function create();
}
