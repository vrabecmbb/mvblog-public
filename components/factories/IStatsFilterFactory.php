<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface IStatsFilterFactory {
    
    /** @return \Components\StatsFilter */
    public function create();
}
