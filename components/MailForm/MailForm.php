<?php

namespace Components;

use Nette\Forms\Form,
    Nette\Mail\Message,
    Nette\Mail\SendmailMailer;

class MailForm extends BaseControl{

  /** @var array */
  public $onError = [];

  /** @var array */
  public $onSuccess = [];

  public function createComponentForm(){
    $form = $this->formFactory->create();

    $form->addText("mail_name", "Meno:")
      ->setRequired("Zadajte prosím meno!")
    ;

    $form->addText("mail_email", "E-mail:")
        ->addCondition(Form::FILLED)
        ->addRule(Form::EMAIL, "Zadajte správny email!")
    ;

    $form->addText("mail_subject", "Predmet:")
      ->setRequired("Zadajte prosím predmet správy!")
    ;

    $form->addTextArea("mail_message", "Správa:")
      ->setRequired("Zadajte prosím predmet správy!")
    ;

    $form->addSubmit("mail_send", "Odoslať");

    $form->onSuccess[] = [$this, 'sendMailForm'];

    return $form;
  }

  public function sendMailForm($form){
    $values = $form->getValues();

    $params = [
	  'wwwDir' => WWW_DIR,
      'name' => $values->mail_name,
      'subject' => $values->mail_subject,
      'mail' => $values->mail_email,
      'message' => $values->mail_message,
      'time' => time()
    ];

	$latte = new \Latte\Engine;

    $mail = new Message;
    $mail
      ->setFrom("MVonline info <info@mvonline.eu>")
      ->addTo("martin.vrabec@hotmail.com")
      ->setSubject("MVonline Blog - " .$values->mail_subject)
      ->setHtmlBody($latte->renderToString(__DIR__.'/email.latte', $params))
    ;

    $mailer = new SendmailMailer;
    $mailer->send($mail);
    
    $this->fireCallbacks($this->onSuccess,'Mail bol úspešne odoslaný');
  }

  public function render(){
    $this->template->setFile(__DIR__.'/template.latte');
    $this->template->render();
  }

}
