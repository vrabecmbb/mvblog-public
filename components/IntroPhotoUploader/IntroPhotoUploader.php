<?php

namespace Components;

use Nette\Forms\Form;

class IntroPhotoUploader extends BaseControl{
	
	
	protected function createComponentForm() {
		$form = $this->formFactory->create();
		
		$form->addUpload('imgIntro',NULL)
				->addCondition(Form::FILLED)
				->addRule(Form::IMAGE, "Súbor musí byť obrázok")
		;
		
		$form->onSuccess[] = $this->processForm();
		
		return $form;
	}
	
	public function processForm($form){
		dump($form->getValues());
	}
	
	public function render(){
		$this->template->setFile(__DIR__.'/template.latte');
		
		$this->template->render();
	}
	
}

