<?php

namespace Components;

/**
 * Description of BaseControl
 *
 * @author Administrator
 */
abstract class BaseControl extends \Nette\Application\UI\Control
{
    
    /** @var Factories\FormFactory @inject */
	public $formFactory;
        
        public function __construct()
	{
            parent::__construct();
	}

	protected function fireCallbacks($callbacks, $args = [])
	{
		foreach ($callbacks as $callback) {
			call_user_func_array($callback, $args);
		}
	}    
}
