<?php

namespace Components;

/**
 * Description of Breadcrumb
 *
 * @author Administrator
 */
class Breadcrumb extends BaseControl{
	
	/** var array */
	public $links = [];
	
	public function addCrumb($title, $link = NULL){
		$this->links[md5($title)] = [
			'title' => $title,
			'link' => $link
		];
	}
	
	public function clearCrumbs(){
		$this->links = NULL;
	}
	
	public function render()
	{
		$this->template->links = $this->links;
		$this->template->setFile(__DIR__.'/template.latte');
		$this->template->render();
	}
	
}
