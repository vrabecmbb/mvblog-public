<?php

namespace App;

use Nette,  
        App\Model\PanoramaModel,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{               
	/**
	 * @return \Nette\Application\IRouter
	 */
    
	public function createRouter()
        {
            $router = new RouteList();

            $router[] = $adminRouter = new RouteList('Admin');
            $adminRouter[] = new Route('admin/<presenter>/<action>/[<id>]', 'Homepage:default');

            /*$router[] = $frontRouter = new RouteList('Front');
            $frontRouter[] = new Route('panoramy/show[/<id>]',array(
                'presenter'=>'Panoramy',
                'action'=>'show',
                'id'=>array(
                    Route::FILTER_IN => function($id) use ($database){
                        $retVal = $database->table('panorama_images')->select('id')->where(array('slug'=>$id))->fetch();
                        
                        if(!$retVal)
                            return 'a';
                        
                        return $retVal->id;
                    },        
                    Route::FILTER_OUT => function($id) use ($database){
                        $retVal = $database->table('panorama_images')->select('slug')->where(array('id'=>$id))->fetch();
                       
                        if(!$retVal)
                            return 'a';
                        
                       return $retVal->slug;
                    })
            ));*/   
			
			//$router[] = new \Nette\Application\Routers\CliRouter(["action" => 'Cli:Cli:cron']);
			$router[] = $cliRouter = new RouteList('Cli');
			$cliRouter[] = new Route('cli/<action>[/<id>]', 'Cli:cron');
			
			$router[] = $frontRouter = new RouteList('Front');
            
			$frontRouter[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
			
			$frontRouter[] = new Route('<presenter>[/<action>]/<month>/<year>', 'Homepage:default');
           
            
            
            return $router;
        }

}
