<?php

namespace App\CliModule\Presenters;

use Nette;
use Nette\Caching\Cache;

class CliPresenter extends \Nette\Application\UI\Presenter {
	
	/** @var \Flickr\FlickrManager @inject */
	public $flickrManager;

    public function actionCron() {
		$this->writeAccess();
		
		$cache = new Cache(new Nette\Caching\Storages\FileStorage(APP_DIR.'/../temp'));
		
		$flickr_past = file_get_contents('nette.safe://flickrcache.txt');
		
		//calculate md5 from array of galeries
		$flickr = $this->flickrManager->getPhotos(12);
		$flickr_md5 = md5(\Nette\Utils\Json::encode($flickr));
		
		$file = fopen('nette.safe://flickrcache.txt','w+');
		
		if ($flickr_past !== $flickr_md5){
			$cache->save('flickrPhotos', $flickr);
			fwrite($file, $flickr_md5);
		}
		
		fclose($file);

        $this->terminate();
    }
	
	private function writeAccess(){
		$d = new \DateTime;
		$unix = $d->getTimestamp();
		
		$file = fopen('nette.safe://crontime.txt','w+');
		fwrite($file, $unix);
		fclose($file);
	}
}