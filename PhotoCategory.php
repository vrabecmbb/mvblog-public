<?php

namespace App;

use Doctrine\ORM\Mapping as ORM,
    Nette\Utils\Json;

/**
 * @ORM\Entity
 */
class PhotoCategory extends \Kdyby\Doctrine\Entities\BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $category_id;

    /**
     * @ORM\Column(type="string")
     */
    public $category_name;
    // ...
    /**
     * @ORM\OneToMany(targetEntity="PhotoCategory", mappedBy="parent")
     * */
    public $children;

    /**
     * @ORM\ManyToOne(targetEntity="PhotoCategory", inversedBy="children")
     * @ORM\JoinColumn(name="category_parent", referencedColumnName="category_id")
     * */
    public $parent;

    /**
     * @ORM\Column (type="string") 
     */
    public $photogallery;

    public function __toString() {
        return $this->category_name;
    }

    public function getId() {
        return $this->category_id;
    }

    public function getName() {
        return $this->category_name;
    }

    /**
     * @return \App\PhotoCategory
     */
    public function getParent() {
        return $this->parent;
    }

    public function getAllCategories() {
        return $this->findAll();
    }

    public function getPhoto() {
        return Json::decode($this->photogallery);
    }

    /*     * *
     *  SETTERS
     */

    public function setName($name) {
        $this->category_name = $name;
    }

    public function setParentId($parent) {
        if ($parent instanceof PhotoCategory) {
            $this->parent = $parent;
        } elseif (!is_null ($parent)) {
            throw new \Exception('Parent is not instance of PhotoCategory');
        }
    }

    public function setPhotogallery($photogallery) {
        if (!is_array($photogallery)) {
            throw new Exception('Photogallery is not array');
        } else {
            $photogallery = \Nette\Utils\Json::encode($photogallery);
            $this->photogallery = $photogallery;
        }
    }

}
